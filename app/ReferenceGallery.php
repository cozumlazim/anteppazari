<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReferenceGallery extends Model
{
    protected $table="reference_gallery";
    protected $fillabled = [
        'reference_id', 'image','status',
    ];
}
