<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Reference;
use App\ReferenceGallery;
use Auth;
use Carbon\Carbon;
use DB;
use Session;
use Laracasts\Flash\Flash;
use App\Helpers\Helper;


class ReferenceController extends Controller
{
   public function __construct(Request $request)
    {
        $url = $request->path();
        Helper::sessionReload();
        $sess= Helper::shout($url);
        $this->read=$sess['r'];
        $this->update=$sess['u'];
        $this->add=$sess['a'];
        $this->delete=$sess['d'];
        $this->sess=$sess;
    }
    public function index(){
        if($this->read==0){
            return redirect()->action('Admin\HomeController@index');
        }
    	$references = Reference::all();

    	return view('admin.reference.index', ['references' => $references, 'deleg' => $this->sess]);

    }
    public function create(){
        if($this->read==0 || $this->add==0){
            return redirect()->action('Admin\HomeController@index');
        }
        return view('admin.reference.create');
    }

    public function save(Request $request){
        if($request->file('image') != null) {
            $path = base_path() . '/public/img/reference';

            $imageTempName = $request->file('image')->getPathname();
            $current_time = time();
            $imageName = $current_time."_".$request->file('image')->getClientOriginalName();

            $request->file('image')->move($path , $imageName);
            $newresim = '/img/reference/'.$imageName;
        }
        else{
            $newresim="";
        }

        $reference = new Reference();
        $reference->name = $request->input('name');
        $reference->refcontent = $request->input('refcontent');
        $reference->priority = $request->input('priority');
        $reference->image = $newresim;
        $reference->link = $request->input('link');
        $reference->status = $request->input('status');
        $reference->save();
        Flash::message('Referans başarılı bir şekilde eklendi.','success');
        return redirect('/admin/reference');
    }

    public function saveGallery(Request $request){
        $path = base_path() . '/public/img/reference';
    
        $imageTempName = $request->file('image')->getPathname();
        $current_time = time();
        $imageName = $current_time."_".$request->file('image')->getClientOriginalName();
    
        $request->file('image')->move($path , $imageName);
        $newresim = '/img/reference/'.$imageName;

        $reference = new ReferenceGallery();
        $reference->reference_id = $request->input('reference_id');
        $reference->image = $newresim;
        $reference->status = 1;
        $reference->save();
        Flash::message('Referans Galeri başarılı bir şekilde eklendi.','success');
        return redirect('/admin/reference/edit/'.$request->input('reference_id'));
    }

    public function update(Request $request)
    {
        if($request->file('image') != null) {
            $path = base_path() . '/public/img/reference';

            $imageTempName = $request->file('image')->getPathname();
            $current_time = time();
            $imageName = $current_time."_".$request->file('image')->getClientOriginalName();

            $request->file('image')->move($path , $imageName);
            $newresim = '/img/reference/'.$imageName;
        }
        else{
            $spreference = Reference::where('id', $request->input('id'))->first();
            $newresim = $spreference->image;
        }
        $reference = Reference::find($request->input('id'));
        $reference->name = $request->input('name');
        $reference->refcontent = $request->input('refcontent');
        $reference->priority = $request->input('priority');
        $reference->image = $newresim;
        $reference->link = $request->input('link');
        $reference->status = $request->input('status');
        $reference->save();        
        return redirect('/admin/reference'); 
        Flash::message('Referans başarılı bir şekilde güncellendi.','success');
    }

    public function edit($id)
    {
        if($this->read==0 || $this->update==0){
            return redirect()->action('Admin\HomeController@index');
        }
        $allReference = Reference::all();
        $reference = Reference::find($id);
        $referenceGallery = ReferenceGallery::where('status', 1)->get();
        return view('admin.reference.edit',['reference' => $reference, 'allReference' => $allReference, 'referenceGallery' => $referenceGallery]);
    }

    

    public function delete($id) {
        if ($this->read==0 || $this->delete==0) {
            return redirect()->action('Admin\HomeController@index');
        }
        $reference=Reference::find($id);
        $reference->status=0;
        $reference->save();
        return redirect()->back();
    }

    public function deleteGallery($id) {
        if ($this->read==0 || $this->delete==0) {
            return redirect()->action('Admin\HomeController@index');
        }
        $reference=ReferenceGallery::where('id', $id)->first();
        $reference->status=0;
        $reference->save();
        return redirect()->back();
    }

}
