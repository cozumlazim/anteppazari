<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Seller;
use Auth;
use Carbon\Carbon;
use DB;
use Session;
use Laracasts\Flash\Flash;
use App\Helpers\Helper;

class SellerController extends Controller
{
    public function __construct(Request $request)
    {
        $url = $request->path();
        Helper::sessionReload();
        $sess= Helper::shout($url);
        $this->read=$sess['r'];
        $this->update=$sess['u'];
        $this->add=$sess['a'];
        $this->delete=$sess['d'];
        $this->sess=$sess;
    }
    public function index(){
        if($this->read==0){
            return redirect()->action('Admin\HomeController@index');
        }
        $sellers = Seller::all();

        return view('admin.seller.index', ['sellers' => $sellers, 'deleg' => $this->sess]);

    }
    public function create(){
        if($this->read==0 || $this->add==0){
            return redirect()->action('Admin\HomeController@index');
        }
        return view('admin.seller.create');
    }

    public function save(Request $request){

        $path = base_path() . '/public/img/seller';

        $imageTempName = $request->file('image')->getPathname();
        $current_time = time();
        $imageName = $current_time."_".$request->file('image')->getClientOriginalName();

        $request->file('image')->move($path , $imageName);
        $newresim = '/img/seller/'.$imageName;

        $seller = new Seller();
        $seller->name = $request->input('name');
        $seller->priority = $request->input('priority');
        $seller->image = $newresim;
        $seller->link = $request->input('link');
        $seller->status = $request->input('status');
        $seller->save();
        Flash::message('Referans başarılı bir şekilde eklendi.','success');
        return redirect('/admin/seller');
    }

    public function update(Request $request)
    {
        $path = base_path() . '/public/img/seller';

        $imageTempName = $request->file('image')->getPathname();
        $current_time = time();
        $imageName = $current_time."_".$request->file('image')->getClientOriginalName();

        $request->file('image')->move($path , $imageName);
        $newresim = '/img/seller/'.$imageName;
        $seller = Seller::where('id', $request->input('id'))->first();
        $seller->name = $request->input('name');
        $seller->priority = $request->input('priority');
        $seller->image = $newresim;
        $seller->link = $request->input('link');
        $seller->status = $request->input('status');
        $seller->save();
        Flash::message('Referans başarılı bir şekilde güncellendi.','success');
        return redirect('/admin/seller');
    }

    public function edit($id)
    {
        if($this->read==0 || $this->update==0){
            return redirect()->action('Admin\HomeController@index');
        }
        $allReference = Reference::all();
        $reference = Reference::find($id);
        return view('admin.reference.edit',['reference' => $reference, 'allReference' => $allReference]);
    }



    public function delete($id) {
        if ($this->read==0 || $this->delete==0) {
            return redirect()->action('Admin\HomeController@index');
        }
        $reference=Reference::find($id);
        $reference->status=0;
        $reference->save();
        return redirect()->back();
    }
}
