<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Illuminate\Http\Request;
use DB;
use Auth;
use App\User;
use App\Product;
use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use App\Categories;
use App\Usefor;


class ProductController extends Controller
{
    public function __construct(Request $request)
    {
        $url = $request->path();
        Helper::sessionReload();
        $sess= Helper::shout($url);
        $this->read=$sess['r'];
        $this->update=$sess['u'];
        $this->add=$sess['a'];
        $this->delete=$sess['d'];
        $this->sess=$sess;
    }
    //Tüm Ürünler
    public function index(){
        if($this->read==0){
            return redirect()->action('Admin\HomeController@index');
        }
    	//$products = Product::all();
        $products = DB::table('products')
                    ->join('users', 'products.author', '=', 'users.id')
                    ->join('emc_categories', 'products.category_id', '=', 'emc_categories.id')
                    ->select('products.*','users.name as username','emc_categories.title as cat_title')
                    ->orderBy('products.priority')
                    ->get();
    	return view('admin.product.index')->with(['products' => $products,'deleg' => $this->sess]);
    }
    //Yeni Ürünler
    public function create(){
        if($this->read==0 || $this->add==0){
            return redirect()->action('Admin\HomeController@index');
        }
        $allCategories = Categories::where('type', 'urun')->get();
    	return view('admin.product.create')->with(['allCategories' => $allCategories]);
    }

    //Yeni Ürün Oluşturma Fonksiyonu
    public function save(Request $request){
        if($request->file('image') != null) {
            $path = base_path() . '/public/img/urunler';
            $imageTempName = $request->file('image')->getPathname();
            $current_time = time();
            $imageName = $current_time."_".$request->file('image')->getClientOriginalName();
            $request->file('image')->move($path , $imageName);
            $newresim = '/img/urunler/'.$imageName;
        }
        else{
            $newresim="";
        }

        if($request->file('second_image') != null) {
            $path = base_path() . '/public/img/urunler';
            $imageTempName = $request->file('second_image')->getPathname();
            $current_time = time();
            $imageName = $current_time."_".$request->file('second_image')->getClientOriginalName();
            $request->file('second_image')->move($path , $imageName);
            $newscresim = '/img/urunler/'.$imageName;
        }
        else{
            $newscresim="";
        }

    	$product = new Product();
        $product->author = Auth::user()->id;
        $product->name = $request->input("name");
        $product->content = $request->input("content");
        $product->price = $request->input("price");
        $product->stock = $request->input("stock");
        $product->description = $request->input("description");
        $product->priority = $request->input("priority");
        $product->tags = $request->input("tags");
        $product->category_id = $request->input("cat_id");
        $product->image = $newresim;
        $product->second_image = $newscresim;
        $product->slug = str_slug($request->input("name"));
        $product->status = 1;
        $product->save();
    	return redirect('/admin/product/tum-urunler');
    }

    public function edit(Request $request){
        if($this->read==0 || $this->update==0){
            return redirect()->back();
        }
    	$id = $request->id;
    	$product = Product::find($id);
        $allCategories = Categories::where('type', 'urun')->get();
        $allUsefor = Usefor::where('product_id', $id)->get();
    	return view('admin.product.edit')->with(['product' => $product, 'allCategories' => $allCategories, 'allUsefor' => $allUsefor]);
    }
    
    // Ürün Güncelleme Fonksiyonu
    public function update(Request $request){        
        $id = $request->input("id");
		$productData = Product::find($id);
        if($request->file('image') != null) {
            $path = base_path() . '/public/img/urunler';
            $imageTempName = $request->file('image')->getPathname();
            $current_time = time();
            $imageName = $current_time."_".$request->file('image')->getClientOriginalName();
            $request->file('image')->move($path , $imageName);
            $newresim = '/img/urunler/'.$imageName;
        }
        else{
            $newresim=$productData->image;
        }

        if($request->file('second_image') != null) {
            $path = base_path() . '/public/img/urunler';
            $imageTempName = $request->file('second_image')->getPathname();
            $current_time = time();
            $imageName = $current_time."_".$request->file('second_image')->getClientOriginalName();
            $request->file('second_image')->move($path , $imageName);
            $newscresim = '/img/urunler/'.$imageName;
        }
        else{
            $newscresim=$productData->second_image;
        }

        $productData->name = $request->input("name");
        $productData->content = $request->input("content");
        $productData->price = $request->input("price");
        $productData->stock = $request->input("stock");
        $productData->description = $request->input("description");
        $productData->urun_bilgileri = $request->input("urun_bilgileri");
        $productData->priority = $request->input("priority");
        $productData->tags = $request->input("tags");
        $productData->category_id = $request->input("cat_id");
        $productData->image = $newresim;
        $productData->second_image = $newscresim;        
        $productData->slug = str_slug($request->input("name"));
        $productData->save();
    	return redirect()->back();
    }
    public function updateFaydalari(Request $request){        
        $id = $request->input("product_id");
        $productData = Product::find($id);        
        $productData->fayda_content = $request->input("fayda_content");
        $productData->save();
        return redirect()->back();
    }
    public function updateTarihce(Request $request){        
        $id = $request->input("product_id");
        $productData = Product::find($id);        
        $productData->tarih_content = $request->input("tarih_content");
        $productData->save();
        return redirect()->back();
    }
    // Ürün Silme Fonksiyonu
    public function delete(Request $request){
        if ($this->read==0 || $this->delete==0) {
            return redirect()->action('Admin\HomeController@index');
        }
    	$id = $request->id;
    	$productData = Product::find($id);
		$productData->delete();
		return redirect()->back();
    }
    //ürünü taslak olarak Oluşturma Fonksiyonu
    public function draft(Request $request){
        $product = new Product();
        $product->name = $request->input("name");
        $product->content = $request->input("content");
        $product->price = $request->input("price");
        $product->stock = $request->input("stock");
        $product->description = $request->input("description");
        $product->priority = $request->input("priority");
        $product->tags = $request->input("tags");
        $product->image = $request->input("image");
        $product->status = 2;
        $product->save();
        return redirect('/admin/product/tum-urunler');
    }

    public function updateDraft(Request $request){
        $id = $request->input("id");
        $productData = Product::find($id);
        $productData->name = $request->input("name");
        $productData->content = $request->input("content");
        $productData->price = $request->input("price");
        $productData->stock = $request->input("stock");
        $productData->description = $request->input("description");
        $productData->priority = $request->input("priority");
        $productData->tags = $request->input("tags");
        $productData->image = $request->input("image");
        $productData->status = 2;
        $productData->save();
        return redirect('/admin/product/tum-urunler');
    }

    public function kullanimAlanlari(Request $request)
    {
       if($request->file('kullanim_resim') != null) {
            $path = base_path() . '/public/img/kullanim-alanlari';
            $imageTempName = $request->file('kullanim_resim')->getPathname();
            $current_time = time();
            $imageName = $current_time."_".$request->file('kullanim_resim')->getClientOriginalName();
            $request->file('kullanim_resim')->move($path , $imageName);
            $newresim = '/img/kullanim-alanlari/'.$imageName;
        }
        else{
            $newresim="";
        }

        $usefor = new Usefor();
        $usefor->product_id = $request->input('product_id');
        $usefor->image = $newresim;
        $usefor->title = $request->input('kullanim_alani');
        $usefor->status = "1";
        $usefor->save();

        return redirect()->back();
        
    }
}
