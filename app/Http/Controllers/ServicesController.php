<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use DB;
use Session;
use App\UserDelegation;
use App\Helpers\Helper;
use App\Modules;
use App\Page;
use App\Service;
use App\Product;
use App\Categories;
use App\Usefor;

class ServicesController extends Controller
{
    /*public function index(){
    	$services=Service::where('status','1')->orderBy('priority','asc')->get();
    	$pageService=Page::where('status','1')->where('slug','neler-yapiyoruz')->get();
    	return view('template0.services',['services'=>$services,'pageService'=>$pageService]);
    }*/

	public function servis(){
		$product=Product::where('status','1')->orderBy('priority','asc')->get();
		return view('template1.servis', ['product' => $product]);
	}

	public function show($slug){
		$product=Product::where('status','1')->where('slug', $slug)->first();
		$categories = Categories::where('id', $product->category_id)->first();
		$usefor = Usefor::where('product_id', $product->id)->get();
		return view('template1.show', ['product' => $product, 'categories' => $categories, 'usefor' => $usefor]);
	}

	/*public function metalEnjeksiyon(){
		$pageService=Page::where('status','1')->where('slug','neler-yapiyoruz')->get();
		return view('template1.metal_enjeksiyon', ['pageService'=>$pageService]);
	}

	public function yedekParca(){
		return view('template1.yedek_parca');
	}

	public function urunDetay30Ton($id=""){
		return view('template1.urun_detay_30ton', ['urun'=>$id]);
	}

	public function urunDetay120Ton($id=""){
		return view('template1.urun_detay_120ton', ['urun'=>$id]);
	}

	public function urunDetay200Ton($id=""){
		return view('template1.urun_detay_200ton', ['urun'=>$id]);
	}

	public function urunDetay400Ton($id=""){
		return view('template1.urun_detay_400ton', ['urun'=>$id]);
	}*/

	/*public function revizyon(){
		return view('template1.revizyon');
	}

	public function ikinciEl(){
		return view('template1.ikinciel');
	}*/
}
