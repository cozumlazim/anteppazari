<?php

namespace App\Http\Controllers;


use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use DB;
use Session;
use App\UserDelegation;
use App\Helpers\Helper;
use App\Modules;
use App\Page;
use App\Blog;
use App\Seller;
use App\Service;
use App\Slider;
use App\OurClients;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /*public function __construct()
    {
        $this->middleware('auth');
    }*/

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
        
     

    public function index()
    {

        $hak=Page::where('status','1')->get();
        $blog=Blog::where('status','1')->get();
        $services=Service::where('status','1')->orderBy('priority','asc')->limit(9)->get();
        $comment=OurClients::where('status','1')->orderBy('priority','asc')->get();
        $sellers=Seller::where('status','1')->orderBy('priority','asc')->get();
        $sliders=Slider::where('status','1')->orderBy('priority','asc')->get();
        $products = DB::table('products')
                    ->join('users', 'products.author', '=', 'users.id')
                    ->join('emc_categories', 'products.category_id', '=', 'emc_categories.id')
                    ->select('products.*','users.name as username','emc_categories.title as cat_title')
                    ->get();
        return view('template1.home',['hak'=>$hak,'blog'=>$blog,'services'=>$services,'comment'=>$comment,'sellers'=>$sellers,'sliders'=>$sliders,'products'=>$products]);
    }
    public function showPage($slug){
        $page=Page::where('slug',$slug)->where('status','1')->first();
        if (isset($page)) {
            return view('template0.page',['page'=>$page]);
        }
        else {
            return redirect()->action('HomeController@index');
        }
        
    }

    public function web(){
        return view('welcome');
    }
    public function testBot(){
        function siteConnect($site)
        {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $site);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $site = curl_exec($ch);
            curl_close($ch);
            return $site;
        }
        $giris = siteConnect('http://www.eczanebilgileri.com/Eczane_ilce.asp?id=389');
        preg_match_all('@<p class="hs">(.*?)</p>@si',$giris,$test);
        foreach($test[1] as $value)
        {
            //$value = str_replace("Eczane_detay.asp?id=","",$value);
            echo $value."<br>";
        }
        echo "<pre>";
        print_r($test);
        echo "</pre>";
        /*$giris = siteConnect('http://www.eczanebilgileri.com/');
        preg_match_all('@<p class="category"><a href="(.*?)">@si',$giris,$test);*/
        /*foreach($test[1] as $value)
        {
            $value = str_replace("eczane_il.asp?id=","",$value);
            echo $value."<br>";
            $ilceler = siteConnect('http://www.eczanebilgileri.com/eczane_il.asp?id='.$value);
            preg_match_all('@<p class="category"><a href="(.*?)">@si',$ilceler,$ilces);
            foreach($ilces[1] as $ilce)
            {
                $ilce = str_replace("Eczane_ilce.asp?id=","",$ilce);
                echo $ilce."<br>";
            }*/
            /*$asd = str_split($value);;
            echo "<pre>";
            print_r($asd);
            echo "</pre>";*/
        /*}*/


    }
    public function bot(){
        function siteConnect($site)
        {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $site);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $site = curl_exec($ch);
            curl_close($ch);

            return $site;

        }
        function siteConnectImage($site,$image="")
        {

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $site);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            if($image!="")
            {
                $dosya = fopen(public_path()."/uploads/places/".$image.".jpg","w");
                curl_setopt($ch, CURLOPT_FILE, $dosya);
            }
            $site = curl_exec($ch);
            curl_close($ch);

            return $dosya;

        }

        $giris = siteConnect('https://www.zomato.com/tr/istanbul/populer-mekanlar');
        preg_match_all('@<div class="res_title zblack bold " title="(.*?)">@si',$giris,$baslik);
        preg_match_all('@<div class="nowrap grey-text fontsize4">(.*?)</div>@si',$giris,$semt);
        preg_match_all('@<a href="(.*?)" class="jumbo_track_collections"@',$giris,$link);
        /*preg_match_all('@<div class="ptop0 pbot0">

                      <a href="(.*?)" class="jumbo_track_collections" data-link-type="restaurant">@si',$giris,$link);*/
        $i=0;
        foreach($link['1'] as $value)
        {
            $canliMuzik = 0;
            $wifi = 0;
            $alcol = 0;
            $editLink = explode("/",$value);
            $linkutf = urlencode($editLink['5']);
            $sonLink = $editLink['0']."//".$editLink['2']."/".$editLink['3']."/".$editLink['4']."/".$linkutf;
            $giris2 = siteConnect($sonLink);
            preg_match_all('@<span tabindex="0" aria-label="(.*?)" class="tel" itemprop="telephone">@si',$giris2,$phone);
            preg_match('@<div class="resinfo-icon">(.*?)</div>@si',$giris2,$address);
            preg_match_all('@<td class="pl10" >(.*?)</td>@si',$giris2,$workHour);
            preg_match('@<span class="subtext">(.*?)</span>@si',$giris2,$alkol);
            preg_match_all('@<div class="res-info-feature-text">(.*?)</div>@si',$giris2,$ozellik);
            preg_match_all('@<a href="'.$value.'" data-link-type="restaurant" class="relative lazy top-res-box-bg pl10 ptop0" data-original="(.*?)"@si',$giris,$imageLink);
            echo $sonLink."<br>";
            echo"<pre>";
            /*print_r($link);*/
            print_r($imageLink);
            echo "</pre>";
            echo $i;
            $image = $imageLink['1']['0'];
            $image=ltrim($image,"(");
            $image=rtrim($image,")");
            $resname = str_slug($baslik['1'][$i]);
            $giris3 = siteConnectImage($image, $resname);
            //$resname = "deneme";

            if(isset($phone['1']['0']))
            {
                $phone = $phone['1']['0'];
            }
            else
            {
                $phone = "";
            }

            foreach($ozellik['1'] as $ozellikValue)
            {
                if($ozellikValue=="Canlı Mü<zik")
                {
                    $canliMuzik="Canlı Müzik";
                }
                elseif($ozellikValue=="Wifi")
                {
                    $wifi="Wireless";
                }
            }

            if(isset($alkol))
            {
                $alcol="Alkol";
            }
            $optionsAll = array(
                'pazartesi' => $workHour['1']['0'],
                'sali' => $workHour['1']['1'],
                'carsamba' => $workHour['1']['2'],
                'persembe' => $workHour['1']['3'],
                'cuma' => $workHour['1']['4'],
                'cumartesi' => $workHour['1']['5'],
                'pazar' => $workHour['1']['6'],
                "crediCard" => '',
                "alkol" => $alcol,
                "canliMuzik" => $canliMuzik ,
                "parkDurumu" => '0',
                "kupon" => '0',
                "tekSandalye" => '0',
                "wifi" => $wifi,
                "evcilHayvan" => '0',
                "cocuklar" => '0'
            );
            $resimpath = "/uploads/places/".$resname;
            $image = array(
                "url" => $resimpath
            );
            $optionsAll = json_encode($optionsAll);
            $image = json_encode($image);

            $places = new Place();
            $places->title = $baslik['1'][$i];
            $places->address = $semt['1'][$i];
            $places->country = "Türkiye";
            $places->city = "İstanbul";
            $places->slug = str_slug($baslik['1'][$i]);
            $places->options = $optionsAll;
            $places->phone = $phone;
            $places->image = $image;
            $places->save();
            $i++;
        }
        /*foreach($baslik['1'] as $value)
        {
            echo $value."<br>";
        }*/
        /*echo $baslik['1']['5'];
        echo "<pre>";
        print_r($baslik);
        print_r($semt);
        echo "</pre>";*/
    }

}
