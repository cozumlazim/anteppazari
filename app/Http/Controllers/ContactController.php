<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use DB;
use Session;
use App\UserDelegation;
use App\Helpers\Helper;
use App\Modules;
use App\Page;
use App\Blog;
use App\Reference;
use App\Service;
use App\OurClients;
use Mail;

class ContactController extends Controller
{
    public function index(){
    	return view('template1.contact');
    }

    public function offerMail(Request $request)
    {
    	$data = $request->all();
		$ad = $data['ad'];
		$email = $data ['email'];
		$telefon = $data['telefon'];
		$mesaj = $data['mesaj'];
		$urun = $data['urun'];

        Mail::send('template1.offer', ['ad' => $ad, 'urun' => $urun, 'email' => $email, 'telefon' => $telefon, 'mesaj' => $mesaj], function ($m) {
            $m->from('info@anteppazari.com.tr', 'Teklif Al!');

            $m->to('info@anteppazari.com.tr', 'teklifal')->subject('Teklif Al!');
        });
    }

    public function contactMail(Request $request)
    {
    	$data = $request->all();
		$ad = $data['ad'];
		$konu = $data['konu'];
		$email = $data ['email'];
		$telefon = $data['telefon'];
		$mesaj = $data['mesaj'];


        Mail::send('template1.message', ['ad' => $ad, 'konu' => $konu, 'email' => $email, 'telefon' => $telefon, 'mesaj' => $mesaj], function ($m) {
            $m->from('info@anteppazari.com.tr', 'Bize Ulaşın');

            $m->to('info@anteppazari.com.tr', 'anteppazari')->subject('Bize Ulaşın!');
        });
    }
}
