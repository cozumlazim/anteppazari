<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    protected $table="seller";
    protected $fillabled = [
        'name', 'link','image','status','priority'
    ];
}
