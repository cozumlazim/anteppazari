<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //Ürünler
    protected $fillabled = [
    	'name', 'description','content','image', 'second_image','stock','price','tags','options','priority','display', 'fayda_content', 'tarih_content', 'slug', 'urun_bilgileri',
    ]; 
}
