@extends('admin.master')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Ürünler
    <small>Ürün Ekle</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="/admin"><i class="fa fa-dashboard"></i> Anasayfa</a></li>
    <li><a href="/admin/product"><i class="fa fa-dashboard active"></i> Ürün Düzenle</a></li>
  </ol>
</section>
<!-- Main content -->
<section class="content">
<div class="row">
  <div class="col-lg-8 col-md-8">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Ürün Düzenle</h3>
      </div><!-- /.box-header -->
      <!-- form start -->
      <form action="/admin/product/update-product" method="POST" enctype="multipart/form-data">
      <input type="hidden" name="id" value="{{$product->id}}">
      {!! csrf_field() !!}
        <div class="box-body">
          <div class="form-group">
            <label for="exampleInputEmail1">Ürün Adı</label>
            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="name" value="{{$product->name}}">
          </div>
          <div class="form-group">
                <label for="exampleInputEmail1">Ürün İçeriği</label>
                <textarea name="content" class="textarea product-text" placeholder="" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$product->content}}</textarea>
          </div>
          <div class="form-group" style="display:none;">
            <div class="row">
              <div class="col-lg-6 col-md-6">
                <label for="exampleInputEmail1">Ürün Fiyatı</label>
                <input type="number" class="form-control" id="exampleInputEmail1" placeholder="" value="{{$product->price}}" name="price">
              </div>
              <div class="col-lg-6 col-md-6">
                <label for="exampleInputEmail1">Ürün Stoğu</label>
                <input type="number" class="form-control" id="exampleInputEmail1" placeholder="" value="{{$product->stock}}" name="stock">
              </div>
            </div>  
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">SEO Açıklama</label>
            <textarea type="text" class="textarea" style="width:100%;height:100px;resize:none;" id="exampleInputEmail1" placeholder="" name="description">{{$product->description}}</textarea>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Ürün Bilgileri</label>
            <textarea type="text" class="textarea" style="width:100%;height:100px;resize:none;" id="exampleInputEmail1" placeholder="" name="urun_bilgileri">{{$product->urun_bilgileri}}</textarea>
          </div>

        </div>
      
    </div><!-- /.box -->
  </div>
  <div class="col-lg-4 col-md-4">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Yayınlama Araçları</h3>
      </div><!-- /.box-header -->
      <!-- form start -->
      
        <div class="box-body">
          <div class="form-group">
            <button type="submit" onclick ="return productFormSave();" class="btn btn-primary">Güncelle</button>
          </div>

          <div class="form-group">
            <h3 class="box-title">Sayfa Düzeni</h3>
            
            <label for="exampleInputEmail1">Kategori</label>
            <select class="form-control" name="cat_id">
            @foreach($allCategories as $value)
              @if($value->id == $product->category_id)
              <option value="{{ $value->id }}" selected>{{ $value->title }}</option>
              @else
              <option value="{{ $value->id }}">{{ $value->title }}</option>
              @endif
            @endforeach            
            </select>
            <label for="exampleInputEmail1">Sıralama</label>
            <input type="number" value="{{$product->priority}}" class="form-control" id="exampleInputEmail1" placeholder="" name="priority">
          </div>
          <div class="form-group">
            <h3 class="box-title">Etiketler</h3>
            <p><small>Kelimeler Arasına Virgül Koyunuz</small></p>
            <textarea type="text" class="form-control" style="width:100%;height:150px;overflow: hidden; resize:none;"  id="exampleInputEmail1" placeholder="" name="tags">{{$product->tags}}</textarea>
          </div>
          <div class="form-group">
            <div class="col-md-6 padding-temizle">
              <h3 class="box-title">1.Görsel</h3>
              <input name="image" type="file" id="exampleInputFile">
              <img src="{{$product->image}}" class="img-responsive">
            </div>
            <div class="col-md-6 padding-temizle">
              <h3 class="box-title">2.Görsel</h3>
              <input name="second_image" type="file" id="exampleInputFile">
              <img src="{{$product->second_image}}" class="img-responsive">
            </div>
          </div>
      </form>

    </div><!-- /.box -->
  </div>
  </div>
  <div class="col-lg-12 col-md-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Kullanım Alanları (400px x 250px)</h3>
      </div><!-- /.box-header -->
      <!-- form start -->   
        <form  action="/admin/product/kullanim-alanlari" method="post" enctype="multipart/form-data">
          {!! csrf_field() !!}
          <input type="hidden" name="product_id" value="{{$product->id}}">
          <div class="box-body">
            <!--<form action="/admin/product/kullanim-alanlari" class="dropzone" id="my-awesome-dropzone">{!! csrf_field() !!}<input type="hidden" name="product_id" value="{{$product->id}}"></form>-->
            <div class="form-group">
              <label for="exampleInputEmail1">Resim</label>
              <input type="file" class="form-control" id="inputResim" name="kullanim_resim">
            </div>
            <div class="form-group">
              <label for="exampleInputEmail1">Kullanım Alanı</label>
              <input type="text" class="form-control" id="inputKullanimAlani" name="kullanim_alani">
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary">Ekle</button>
            </div>
            <hr>
            <div class="form-group">
              @foreach($allUsefor as $value)
              <div class="col-md-3" style="text-align:center;">
                <img src="{{$value->image}}" style="height:60px;">
                <h3>{{$value->title}}</h3>
                <a class="button btn btn-danger" onclick="deleteApprove('/admin/product/delete/{{$value->id}}')"><i class="fa fa-trash"></i></a>
              </div>
              @endforeach
            </div>
          </div>
        </form>    
    </div><!-- /.box -->
  </div>
  <div class="col-lg-12 col-md-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Ürün Faydaları</h3>
      </div><!-- /.box-header -->
      <!-- form start -->   
        <form  action="/admin/product/faydalari" method="post">
          {!! csrf_field() !!}
          <input type="hidden" name="product_id" value="{{$product->id}}">
          <div class="box-body">
            <!--<form action="/admin/product/kullanim-alanlari" class="dropzone" id="my-awesome-dropzone">{!! csrf_field() !!}<input type="hidden" name="product_id" value="{{$product->id}}"></form>-->
            <div class="form-group">
              <label for="exampleInputEmail1">Fayda İçeriği</label>
              <textarea name="fayda_content" class="textarea product-text" placeholder="" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$product->fayda_content}}</textarea>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary">Kaydet</button>
            </div>
          </div>
        </form>    
    </div><!-- /.box -->
  </div>
  <div class="col-lg-12 col-md-12">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Ürün Tarihçesi</h3>
      </div><!-- /.box-header -->
      <!-- form start -->   
        <form  action="/admin/product/tarihce" method="post">
          {!! csrf_field() !!}
          <input type="hidden" name="product_id" value="{{$product->id}}">
          <div class="box-body">
            <!--<form action="/admin/product/kullanim-alanlari" class="dropzone" id="my-awesome-dropzone">{!! csrf_field() !!}<input type="hidden" name="product_id" value="{{$product->id}}"></form>-->
            <div class="form-group">
              <label for="exampleInputEmail1">Tarih İçeriği</label>
              <textarea name="tarih_content" class="textarea product-text" placeholder="" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;">{{$product->tarih_content}}</textarea>
            </div>
            <div class="form-group">
              <button type="submit" class="btn btn-primary">Kaydet</button>
            </div>
          </div>
        </form>    
    </div><!-- /.box -->
  </div>
</div>
</section><!-- /.content -->

<script type="text/javascript">
  /*
function productFormSave (){
  $("#productForm").attr("action","/admin/product/update-product");
}
function productFormDraft (){
  $("#productForm").attr("action","/admin/product/update-draft");
}*/
</script>
</div><!-- /.content-wrapper -->
@endsection