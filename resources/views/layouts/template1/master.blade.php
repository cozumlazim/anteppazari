<!DOCTYPE html>
<html dir="ltr" lang="en-US">
<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="csrf_token" content="{{ csrf_token() }}" />
    <meta name="author" content="SemiColonWeb" />

    <!-- Stylesheets
    ============================================= -->
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="/css/template1/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="/css/template1/style.css" type="text/css" />
    <link rel="stylesheet" href="/css/template1/swiper.css" type="text/css" />
    <link rel="stylesheet" href="/css/template1/dark.css" type="text/css" />
    <link rel="stylesheet" href="/css/template1/font-icons.css" type="text/css" />
    <link rel="stylesheet" href="/css/template1/animate.css" type="text/css" />
    <link rel="stylesheet" href="/css/template1/magnific-popup.css" type="text/css" />
    <link rel="stylesheet" href="/plugins/font-awesome/css/font-awesome.min.css" type="text/css" />
    <link rel="stylesheet" href="/css/template1/responsive.css" type="text/css" />
    <link rel="apple-touch-icon" sizes="57x57" href="/img/favicons/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/favicons/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/favicons/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/favicons/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/favicons/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/favicons/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/favicons/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/favicons/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicons/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/img/favicons/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/img/favicons/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicons/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/img/favicons/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    @yield('csscode')
    <!--[if lt IE 9]>
    <script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="/css/template1/main.css" type="text/css" />

    <!-- Document Title
    ============================================= -->
    <title>{{$allSetting->name}}</title>

</head>

<body class="stretched no-transition">

<!-- Document Wrapper
============================================= -->
<div id="wrapper" class="clearfix">

    <!-- Top Bar
        ============================================= -->
        <div id="top-bar" >

            <div class="container clearfix">

                <div class="col_half nobottommargin">

                    <p class="nobottommargin"><strong style=""><i class="fa fa-envelope"></i></strong> info@anteppazari.com.tr</p>

                </div>

                <div class="col_half col_last fright nobottommargin">

                    <!-- Top Links
                    ============================================= -->
                    <div class="top-links">
                        <ul>
                            <li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
                            <li><a href="https://www.instagram.com/anteppazarioffical/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus-square" style="margin-right: 0;"></i></a></li>
                        </ul>
                    </div><!-- .top-links end -->

                </div>

            </div>

        </div><!-- #top-bar end -->

        <!-- Header
        ============================================= -->
        <header class="header" >

            <div id="header-wrap">

                <!-- Header -->
                <div class="container header mobile-menu">
                    <div class="col-md-12 padding-temizle">
                        <a href="/"><img src="/img/template1/anteppazari-logo.png" style="height:76px; margin:auto; position: absolute; top: 0;"></a>
                    </div>
                    <div class="col-md-12 padding-temizle" style="/*position: relative;*/top: -25px;">
                        <nav class="navbar navbar-default">
                          <div class="container-fluid padding-temizle">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                              </button>
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse padding-temizle" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav ">                        
                                    <li class="dropdown">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">ANTEP FISTIĞI</a>
                                      <ul class="dropdown-menu">
                                        <li><a href="/ana-citlak">ANA ÇITLAK</a></li>
                                        <li><a href="/siirt-fistigi">SİİRT FISTIĞI</a></li>
                                        <li><a href="/antep-firik-bozic">ANTEP FİRİK BOZİÇ</a></li>
                                        <li><a href="/antep-file-ic">ANTEP FİLE İÇ</a></li>
                                        <li><a href="/toz-fistik">TOZ FISTIK</a></li>
                                        <li><a href="/antep-kirmizi-ic">ANTEP KIRMIZI İÇ</a></li>
                                      </ul>
                                    </li>
                                    <li class="dropdown">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">BADEM</a>
                                      <ul class="dropdown-menu">
                                        <li><a href="/yerli-badem-ici">YERLİ BADEM İÇİ</a></li>
                                        <li><a href="/yerli-soyulmus-badem-ici">YERLİ SOYULMUŞ BADEM İÇİ</a></li>           
                                      </ul>
                                    </li>
                                    <li class="dropdown">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">CEVİZ</a>
                                      <ul class="dropdown-menu">
                                        <li><a href="/kelebek-ceviz">KELEBEK CEVİZ</a></li>
                                        <li><a href="/kirik-ceviz">KIRIK CEVİZ</a></li>                     
                                      </ul>
                                    </li>
                                    <li class="dropdown mobile-show">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="padding-right: 0px;border:none;">FINDIK</a>
                                      <ul class="dropdown-menu">
                                        <li><a href="/findik-ici">FINDIK İÇİ</a></li>
                                        <li><a href="/file-findik">FİLE FINDIK</a></li>
                                        <li><a href="/pirinc-findik">PİRİNÇ FINDIK</a></li>
                                      </ul>
                                    </li>
                                    <li class="dropdown">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">KARIŞIK</a>
                                      <ul class="dropdown-menu">
                                        <li><a href="/ozel-karisik">KARIŞIK KURUYEMİŞ</a></li>
                                        <li><a href="/luks-karisik">KARIŞIK LÜX KURUYEMİŞ</a></li>
                                      </ul>
                                    </li>
                                    <li class="dropdown">
                                      <a href="/teklif-al">TEKLİF AL</a>                                      
                                    </li>
                                    <li class="dropdown">
                                      <a href="/hakkimizda">HAKKIMIZDA</a>                                      
                                    </li>
                                    <li class="dropdown">
                                      <a href="/iletisim" style="border:none;padding-right: 0;">İLETİŞİM</a>                                  
                                    </li>           
                                </ul>
                            </div><!-- /.navbar-collapse -->
                          </div><!-- /.container-fluid -->
                        </nav>
                    </div>  
                </div>
                <div class="container header web-menu">
                    <div class="col-md-5 padding-temizle" style="/*position: relative;top: -45px;*/">
                        <nav class="navbar navbar-default">
                          <div class="container-fluid padding-temizle">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                              </button>
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse padding-temizle" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav ">                        
                                    <li class="dropdown">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="padding-left: 0px;">ANTEP FISTIĞI</a>
                                      <ul class="dropdown-menu">
                                        <li><a href="/ana-citlak">ANA ÇITLAK</a></li>
                                        <li><a href="/siirt-fistigi">SİİRT FISTIĞI</a></li>
                                        <li><a href="/antep-firik-bozic">ANTEP FİRİK BOZİÇ</a></li>
                                        <li><a href="/antep-file-ic">ANTEP FİLE İÇ</a></li>
                                        <li><a href="/toz-fistik">TOZ FISTIK</a></li>
                                        <li><a href="/antep-kirmizi-ic">ANTEP KIRMIZI İÇ</a></li>
                                      </ul>
                                    </li>
                                    <li class="dropdown">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">BADEM</a>
                                      <ul class="dropdown-menu">
                                        <li><a href="/yerli-badem-ici">YERLİ BADEM İÇİ</a></li>
                                        <li><a href="/yerli-soyulmus-badem-ici">YERLİ SOYULMUŞ BADEM İÇİ</a></li>           
                                      </ul>
                                    </li>
                                    <li class="dropdown">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">CEVİZ</a>
                                      <ul class="dropdown-menu">
                                        <li><a href="/kelebek-ceviz">KELEBEK CEVİZ</a></li>
                                        <li><a href="/kirik-ceviz">KIRIK CEVİZ</a></li>                     
                                      </ul>
                                    </li>
                                    <li class="dropdown mobile-show">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" style="padding-right: 0px;border:none;">FINDIK</a>
                                      <ul class="dropdown-menu">
                                        <li><a href="/findik-ici">FINDIK İÇİ</a></li>
                                        <li><a href="/file-findik">FİLE FINDIK</a></li>
                                        <li><a href="/pirinc-findik">PİRİNÇ FINDIK</a></li>
                                      </ul>
                                    </li>           
                                </ul>
                            </div><!-- /.navbar-collapse -->
                          </div><!-- /.container-fluid -->
                        </nav>
                    </div>
                    <div class="col-md-2 padding-temizle">
                        <a href="/"><img src="/img/template1/anteppazari-logo.png" style="height:190px; margin:auto; position: absolute; top: -40px;"></a>
                    </div>
                    <div class="col-md-5 padding-temizle mobile-none" style="/*position: relative;top: -45px;*/">
                        <nav class="navbar navbar-default" style="float: right;">
                          <div class="container-fluid padding-temizle">
                            <!-- Brand and toggle get grouped for better mobile display
                            <div class="navbar-header">
                              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                              </button>
                            </div> -->

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse padding-temizle" id="bs-example-navbar-collapse-2">
                                <ul class="nav navbar-nav">                        
                                    <li class="dropdown">
                                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">KARIŞIK</a>
                                      <ul class="dropdown-menu">
                                        <li><a href="/ozel-karisik">KARIŞIK KURUYEMİŞ</a></li>
                                        <li><a href="/luks-karisik">KARIŞIK LÜX KURUYEMİŞ</a></li>
                                      </ul>
                                    </li>
                                    <li class="dropdown">
                                      <a href="/teklif-al">TEKLİF AL</a>                                      
                                    </li>
                                    <li class="dropdown">
                                      <a href="/hakkimizda">HAKKIMIZDA</a>                                      
                                    </li>
                                    <li class="dropdown">
                                      <a href="/iletisim" style="border:none;padding-right: 0;">İLETİŞİM</a>                                  
                                    </li>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                          </div><!-- /.container-fluid -->
                        </nav>
                    </div>
                </div>
                <!-- End Header -->

            </div>

        </header><!-- #header end -->

    @yield('content')

    <!-- Footer
    ============================================= -->
    <footer id="footer" class="dark">

            <div class="container">

                <!-- Footer Widgets
                ============================================= -->
                <div class="footer-widgets-wrap clearfix">

                    <div class="col_two_third">

                        <div class="col_one_third">

                            <div class="widget clearfix">

                                <img src="/img/footer-logo.png" alt="" class="footer-logo">

                              

                                <div style="background: url('images/world-map.png') no-repeat center center; background-size: 100%;">
                                    <address>
                                        <strong>Adres:</strong><br>
                                        Nizip fıstıkçılar sitesi C blok NO:13<br><br>
                                        <strong>Adres2:</strong><br>
                                        Rami kuru gıda çarşısı 12.sokak NO:58 Eyüp/İstanbul
                                        <br>

                                    </address>
                                    <strong>Tel:</strong> (+90) 533 778 90 88<br>
                                    <strong>Tel2:</strong> (+90) 212 497 43 40 41<br>
                                    <strong>Fax:</strong> 0212 497 43 42<br>
                                    <strong>Email:</strong> info@anteppazari.com.tr
                                </div>

                            </div>

                        </div>

                        <div class="col_one_third">

                            <div class="widget widget_links clearfix">

                                <h4>Sayfalar</h4>

                                <ul>
                                    <li><a href="#">Antep Fıstığı</a></li>
                                    <li><a href="#">Badem</a></li>
                                    <li><a href="#">Ceviz</a></li>
                                    <li><a href="#">Fındık</a></li>
                                    <li><a href="#">Karışık Yemiş</a></li>
                                    <li><a href="/teklif-al">Teklif Al</a></li>
                                    <li><a href="/hakkimizda">Hakkımızda</a></li>
                                    <li><a href="/iletisim">İletişim</a></li>
                                </ul>

                            </div>

                        </div>

                        <div class="col_one_third col_last">

                            <div class="widget clearfix">
                                <h4>Faydalı Linkler</h4>

                                <div id="post-list-footer">
                                    <div class="spost clearfix">
                                        <div class="entry-c">
                                            <div class="entry-title">
                                                <h4><a href="#">Antep Fıstığının Tarihçesi</a></h4>
                                            </div>
                                            
                                        </div>
                                    </div>

                                    <div class="spost clearfix">
                                        <div class="entry-c">
                                            <div class="entry-title">
                                                <h4><a href="#">Badem'in Tarihçesi</a></h4>
                                            </div>
                                            
                                        </div>
                                    </div>

                                    <div class="spost clearfix">
                                        <div class="entry-c">
                                            <div class="entry-title">
                                                <h4><a href="#">Cevizin Tarihçesi</a></h4>
                                            </div>
                                            
                                        </div>
                                    </div>

                                    <div class="spost clearfix">
                                        <div class="entry-c">
                                            <div class="entry-title">
                                                <h4><a href="#">Fındığın Tarihçesi</a></h4>
                                            </div>
                                            
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>

                    <div class="col_one_third col_last">
                     <div class="widget clearfix">

                    <h4>sosyal medya hesaplarımız</h4>

                        <div class="widget clearfix" style="margin-bottom: -20px;">


                         <div class="row">

                                <div class="col-md-8 clearfix bottommargin-sm">
                                    <a href="#" class="social-icon si-dark si-colored si-facebook nobottommargin" style="margin-right: 10px;">
                                        <i class="icon-facebook"></i>
                                        <i class="icon-facebook"></i>
                                    </a>

                                    <a href="#" class="social-icon si-dark si-colored si-twitter nobottommargin" style="margin-right: 10px;">
                                        <i class="icon-twitter"></i>
                                        <i class="icon-twitter"></i>
                                    </a>

                                    <a href="https://www.instagram.com/anteppazarioffical/" target="_blank" class="social-icon si-dark si-colored si-instagram nobottommargin" style="margin-right: 10px;">
                                        <i class="icon-instagram"></i>
                                        <i class="icon-instagram"></i>
                                    </a>

                                    <a href="#" class="social-icon si-dark si-colored si-google nobottommargin" style="margin-right: 10px;">
                                        <i class="icon-google"></i>
                                        <i class="icon-google"></i>
                                    </a>
                                    
                                </div>
                                

                            </div>



                        </div>



                        <div class="widget subscribe-widget clearfix">

                            <h5>Sizlerde kampanyalarımızdan haberdar olmak isterseniz aşağıdaki bölüme mail adresinizi yazıp göndermeniz yeterlidir.</h5>
                            <div class="widget-subscribe-form-result"></div>
                            <form id="widget-subscribe-form" action="include/subscribe.php" role="form" method="post" class="nobottommargin">
                                <div class="input-group divcenter">
                                    <span class="input-group-addon"><i class="icon-email2"></i></span>
                                    <input type="email" id="widget-subscribe-form-email" name="widget-subscribe-form-email" class="form-control required email" placeholder="Mail adresinizi yazınız">
                                    <span class="input-group-btn">
                                        <button class="btn btn-success" type="submit">Gönder</button>
                                    </span>
                                </div>
                            </form>
                        </div>

                        <div class="widget clearfix" style="margin-bottom: -20px;">

                           
                        </div>

                    </div>

                </div><!-- .footer-widgets-wrap end -->

            </div>
            </div>

            <!-- Copyrights
            ============================================= -->
            <div id="copyrights">

                <div class="container clearfix">

                <div class="col-md-8">
                
                        Antep Pazarı &copy; Copyright 2016. Tüm Hakları Saklıdır.<br>                              
                    
                        Bu web sitesindeki bütün fotoğraflar tescillidir. İzinsiz kullanılması kesinlikle yasaktır.

                </div>

                <div class="col-md-4">
                
                       Design and Software <a href="http://www.cozumlazim.com" target="_blank"> Çözüm Lazım</a>
                </div>


                    

                </div>

            </div><!-- #copyrights end -->

        </footer><!-- #footer end -->

    </div><!-- #wrapper end -->

<!-- Go To Top
============================================= -->
<div id="gotoTop" class="icon-angle-up"></div>

<!-- External JavaScripts
============================================= -->
<script type="text/javascript" src="/js/template1/jquery.js"></script>
<script type="text/javascript" src="/js/template1/plugins.js"></script>

<!-- Footer Scripts
============================================= -->
<script type="text/javascript" src="/js/template1/functions.js"></script>
<script type="text/javascript" src="https://maps.google.com/maps/api/js?key=AIzaSyBeSHmI1J4Cg0aGVTjD2U9-avXFaPx50oo"></script>
<script type="text/javascript" src="/js/template1/jquery.gmap.js"></script>
<script>
    jQuery(document).ready( function($){
        $('#shop').isotope({
            transitionDuration: '0.65s',
            getSortData: {
                name: '.product-title',
                price_lh: function( itemElem ) {
                    if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
                        var price = $(itemElem).find('.product-price ins').text();
                    } else {
                        var price = $(itemElem).find('.product-price').text();
                    }

                    priceNum = price.split("$");

                    return parseFloat( priceNum[1] );
                },
                price_hl: function( itemElem ) {
                    if( $(itemElem).find('.product-price').find('ins').length > 0 ) {
                        var price = $(itemElem).find('.product-price ins').text();
                    } else {
                        var price = $(itemElem).find('.product-price').text();
                    }

                    priceNum = price.split("$");

                    return parseFloat( priceNum[1] );
                }
            },
            sortAscending: {
                name: true,
                price_lh: true,
                price_hl: false
            }
        });

        $('.custom-filter:not(.no-count)').children('li:not(.widget-filter-reset)').each( function(){
            var element = $(this),
                    elementFilter = element.children('a').attr('data-filter'),
                    elementFilterContainer = element.parents('.custom-filter').attr('data-container');

            elementFilterCount = Number( jQuery(elementFilterContainer).find( elementFilter ).length );

            element.append('<span>'+ elementFilterCount +'</span>');

        });

        $('.shop-sorting li').click( function() {
            $('.shop-sorting').find('li').removeClass( 'active-filter' );
            $(this).addClass( 'active-filter' );
            var sortByValue = $(this).find('a').attr('data-sort-by');
            $('#shop').isotope({ sortBy: sortByValue });
            return false;
        });

        $('ul.nav li.dropdown').hover(function() {
          $(this).find('.dropdown-menu').stop(true, true).delay(10).fadeIn(100);
        }, function() {
          $(this).find('.dropdown-menu').stop(true, true).delay(10).fadeOut(100);
        });
    });
</script>
<script type="text/javascript">

    $('#google-map').gMap({
        latitude: 41.049496,
        longitude: 28.916481,
        maptype: 'ROADMAP',
        zoom: 16,
        markers: [
            {
                latitude: 41.049496,
                longitude: 28.916481,
                html: '<div style="width: 300px;"><h4 style="margin-bottom: 8px;">Antep Pazarı</h4></div>',
                icon: {
                    image: "images/icons/map-icon-red.png",
                    iconsize: [32, 39],
                    iconanchor: [32,39]
                }
            }
        ],
        doubleclickzoom: false,
        controls: {
            panControl: true,
            zoomControl: true,
            mapTypeControl: true,
            scaleControl: false,
            streetViewControl: false,
            overviewMapControl: false
        }
    });

</script>
@yield('jscode')
</body>
</html>
