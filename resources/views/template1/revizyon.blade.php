@extends('layouts.template1.master')

@section('content')
<!-- Page Title
    ============================================= -->
<section id="page-title">
    <div class="singleSliderBack" style="background-image: url('/img/template1/cover.jpg');"></div>

    <div class="container clearfix">
        <h1>Revizyon</h1>
        <span>Varol Makine olarak revizyon hizmetimizden yararlanabilirsiniz.</span>
        <ol class="breadcrumb">
            <li><a href="#">Anasayfa</a></li>
            <li class="active">Revizyon</li>
        </ol>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->

<section id="content">

    <div class="content-wrap">


        <div class="container clearfix">
            <!-- Portfolio Single Image
        ============================================= -->
            <div class="fslider" data-pagi="false" data-animation="fade">
                <div class="flexslider">
                    <div class="slider-wrap">
                        <?php
                        for($i=1;$i<15;$i++){
                        ?>
                        <div class="slide"><a href="#"><img src="/img/revizyon/{{ $i }}.jpg" alt="Revizyon"></a></div>
                        <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <!-- Portfolio Single Content
            ============================================= -->
            <div class="" style="margin-top:20px;">

                <div class="">

                    <p>Varol Makine Metal Enjeksiyon, Yedek Parça ve Revizyon Sanayi olarak ömrünü tamamlamış Metal Enjeksiyon tezgahlarınızı tekrar üretime kazandırıyoruz.</p>

                    <div class="col_half nobottommargin">
                        <ul class="iconlist iconlist-color nobottommargin">
                            <li><i class="icon-caret-right"></i> Her tür marka, model ve büyüklükteki metal enjeksiyon makinaları bakım</li>
                            <li><i class="icon-caret-right"></i> Makinanızın orjinal sistemini korayarak revizyon</li>
                            <li><i class="icon-caret-right"></i> Makinanızın sistem revizyonu</li>
                        </ul>
                    </div>

                    <div class="col_half nobottommargin col_last">
                        <ul class="iconlist iconlist-color nobottommargin">
                            <li><i class="icon-caret-right"></i> Her tür marka, model ve büyüklükteki metal enjeksiyon makinaları tamir</li>
                            <li><i class="icon-caret-right"></i> Daha verimli</li>
                            <li><i class="icon-caret-right"></i> Uzman kadromuzla sizlere yardımcı olmaktan mutluluk duyuyoruz.</li>
                        </ul>
                    </div>

                </div>

            </div><!-- .portfolio-single-content end -->

        </div>

    </div>

</section><!-- #content end -->

@endsection