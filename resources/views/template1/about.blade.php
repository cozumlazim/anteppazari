@extends('layouts.template1.master')

@section('content')
<!-- Page Title
    ============================================= -->
<section id="page-title">
    <div class="singleSliderBack" style=""></div>

    <div class="container clearfix">
        <h1>Hakkımızda</h1>
        <!--<span>Antep Pazarı hakkında detaylı bilgiye buradan ulaşabilirsiniz.</span>-->
        <ol class="breadcrumb">
            <li><a href="#">Anasayfa</a></li>
            <li class="active">Hakkımızda</li>
        </ol>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">

            <div class="col-md-12">                

                <div class="heading-block nobottomborder">
                    <h2>EFOR GIDA SANAYİ VE DIŞ TİCARET LİMİTED ŞİRKETİ</h2>
                    <span>Nesilden Nesile Geçen Kalite</span>
                </div>

            </div>

            <div class="clear"></div>

            <div class="col-md-7">
                <span style="text-align:justify;"><?=$aboutus[0]['content']?></span>
                <p style="font-style: italic; font-weight: bold;">Antep Pazarı, Bir Efor Gıda Sanayi ve Dış Ticaret Limited Şirketi Markasıdır.</p>
            </div>

            <div class="col-md-1">
            </div>
            <div class="col-md-4 padding-temizle">
                <img src="/img/template1/anteppazari-logo.png" style="width:58%;margin-bottom:29px;">
                <img src="/img/template1/efor-logo.png" style="width:58%;">
            </div>

        </div>

    </div>

</section><!-- #content end -->

@endsection