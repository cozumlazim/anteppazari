@extends('layouts.template1.master')

@section('content')
<!-- Page Title
    ============================================= -->
<section id="page-title">
    <div class="singleSliderBack" style="background-image: url('/img/template1/cover.jpg');"></div>

    <div class="container clearfix">
        <h1>Yedek Parça</h1>
        <span>Yedek parçalarımıza aşağıdan ulaşabilirsiniz.</span>
        <ol class="breadcrumb">
            <li><a href="/">Anasayfa</a></li>
            <li class="active">Yedek Parça</li>
        </ol>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">

            <div class="col_full">

                <div class="heading-block center nobottomborder">
                    <h2>VAROL MAKİNE METAL ENJEKSİYON, YEDEK PARÇA VE REVİZYON SANAYİ</h2>
                    <span>Uzman ekibimizle sizlere yardımcı olmaktan mutluluk duyuyoruz.</span>
                </div>

                <div class="promo promo-light bottommargin promo-edit">
                    <span class="promo-edit-span" style="text-align:justify;">
                        <p>
                            Varol Makine sizin için en iyisini düşünür. Alanında uzman servis ve yedek parça personelimiz sizi her alanda ayrıcalıklı kılmak için çalışıyor. Geniş yedek parça envanterimiz ile hizmetinizdeyiz.
                        </p>
                        <ul>
                            <li>Hızlı yedek parça ve uzman işçilik.</li>
                            <li>Alanında dünyada kendini kanıtlamış tedarikçilerimiz.</li>
                        </ul>
                    </span>
                </div>

            </div>

            <div class="clear"></div>

            <div class="content-wrap" style="padding:0 !important;">

                <div class="container clearfix">

                    <!-- Portfolio Items
                    ============================================= -->
                    <div id="portfolio" class="portfolio grid-container portfolio-1 clearfix">

                        <article class="portfolio-item pf-media pf-icons clearfix">
                            <div class="portfolio-image">
                                <a href="#">
                                    <img src="/img/template1/yedekparca/yedek-parca-1.jpg" alt="Open Imagination">
                                </a>
                                <div class="portfolio-overlay">
                                    <a href="/img/template1/yedekparca/yedek-parca-1.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
                                </div>
                            </div>
                            <div class="portfolio-desc">
                                <h3><a href="#">Ara Boru</a></h3>
                                <ul class="iconlist">
                                    <li><i class="icon-ok"></i> Sıcak iş çeliğinden imal edilmiştir.</li>
                                    <li><i class="icon-ok"></i> Yüksek çalışma sıcaklıklarına dayanıklı</li>
                                    <li><i class="icon-ok"></i> Darbeye ve eğilmeye dayanıklı</li>
                                    <li><i class="icon-ok"></i> Uzun ömürlü</li>
                                    <li><i class="icon-ok"></i> Dayanıklı dökümden imal edilmiştir.</li>
                                </ul>
                            </div>
                        </article>

                        <article class="portfolio-item pf-illustrations clearfix">
                            <div class="portfolio-image">
                                <a href="#">
                                    <img src="/img/template1/yedekparca/yedek-parca-2.jpg" alt="Locked Steel Gate">
                                </a>
                                <div class="portfolio-overlay">
                                    <a href="/img/template1/yedekparca/yedek-parca-2.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
                                </div>
                            </div>
                            <div class="portfolio-desc">
                                <h3><a href="#">Piston</a></h3>
                                <ul class="iconlist">
                                    <li><i class="icon-ok"></i> Sıcak iş çeliğinden imal edilmiştir.</li>
                                    <li><i class="icon-ok"></i> Yüksek çalışma sıcaklıklarına dayanıklı</li>
                                    <li><i class="icon-ok"></i> Darbeye ve eğilmeye dayanıklı</li>
                                    <li><i class="icon-ok"></i> Uzun ömürlü</li>
                                    <li><i class="icon-ok"></i> Dayanıklı dökümden imal edilmiştir.</li>
                                </ul>
                            </div>
                        </article>

                        <article class="portfolio-item pf-illustrations clearfix">
                            <div class="portfolio-image">
                                <a href="#">
                                    <img src="/img/template1/yedekparca/yedek-parca-3.jpg" alt="Locked Steel Gate">
                                </a>
                                <div class="portfolio-overlay">
                                    <a href="/img/template1/yedekparca/yedek-parca-3.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
                                </div>
                            </div>
                            <div class="portfolio-desc">
                                <h3><a href="#">Nuzzle</a></h3>
                                <ul class="iconlist">
                                    <li><i class="icon-ok"></i> Sıcak iş çeliğinden imal edilmiştir.</li>
                                    <li><i class="icon-ok"></i> Yüksek çalışma sıcaklıklarına dayanıklı</li>
                                    <li><i class="icon-ok"></i> Darbeye ve eğilmeye dayanıklı</li>
                                    <li><i class="icon-ok"></i> Uzun ömürlü</li>
                                    <li><i class="icon-ok"></i> Dayanıklı dökümden imal edilmiştir.</li>
                                </ul>
                            </div>
                        </article>

                        <article class="portfolio-item pf-uielements pf-media clearfix">
                            <div class="portfolio-image">
                                <a href="#">
                                    <img src="/img/template1/yedekparca/yedek-parca-4.jpg" alt="Console Activity">
                                </a>
                                <div class="portfolio-overlay">
                                    <a href="/img/template1/yedekparca/yedek-parca-4.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
                                </div>
                            </div>
                            <div class="portfolio-desc">
                                <h3><a href="#">Piston - Segman - Nuzzle</a></h3>
                                <ul class="iconlist">
                                    <li><i class="icon-ok"></i> Sıcak iş çeliğinden imal edilmiştir.</li>
                                    <li><i class="icon-ok"></i> Yüksek çalışma sıcaklıklarına dayanıklı</li>
                                    <li><i class="icon-ok"></i> Darbeye ve eğilmeye dayanıklı</li>
                                    <li><i class="icon-ok"></i> Uzun ömürlü</li>
                                    <li><i class="icon-ok"></i> Dayanıklı dökümden imal edilmiştir.</li>
                                </ul>
                            </div>
                        </article>

                        <article class="portfolio-item pf-uielements pf-media clearfix">
                            <div class="portfolio-image">
                                <a href="#">
                                    <img src="/img/template1/yedekparca/yedek-parca-5.jpg" alt="Console Activity">
                                </a>
                                <div class="portfolio-overlay">
                                    <a href="/img/template1/yedekparca/yedek-parca-5.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
                                </div>
                            </div>
                            <div class="portfolio-desc">
                                <h3><a href="#">Güzine gömleği</a></h3>
                                <ul class="iconlist">
                                    <li><i class="icon-ok"></i> Sıcak iş çeliğinden imal edilmiştir.</li>
                                    <li><i class="icon-ok"></i> Yüksek çalışma sıcaklıklarına dayanıklı</li>
                                    <li><i class="icon-ok"></i> Darbeye ve eğilmeye dayanıklı</li>
                                    <li><i class="icon-ok"></i> Uzun ömürlü</li>
                                    <li><i class="icon-ok"></i> Dayanıklı dökümden imal edilmiştir.</li>
                                </ul>
                            </div>
                        </article>

                        <article class="portfolio-item pf-uielements pf-media clearfix">
                            <div class="portfolio-image">
                                <a href="#">
                                    <img src="/img/template1/yedekparca/yedek-parca-6.jpg" alt="Console Activity">
                                </a>
                                <div class="portfolio-overlay">
                                    <a href="/img/template1/yedekparca/yedek-parca-6.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
                                </div>
                            </div>
                            <div class="portfolio-desc">
                                <h3><a href="#">Araboru rezistansı</a></h3>
                                <ul class="iconlist">
                                    <li><i class="icon-ok"></i> Sıcak iş çeliğinden imal edilmiştir.</li>
                                    <li><i class="icon-ok"></i> Yüksek çalışma sıcaklıklarına dayanıklı</li>
                                    <li><i class="icon-ok"></i> Darbeye ve eğilmeye dayanıklı</li>
                                    <li><i class="icon-ok"></i> Uzun ömürlü</li>
                                    <li><i class="icon-ok"></i> Dayanıklı dökümden imal edilmiştir.</li>
                                </ul>
                            </div>
                        </article>

                        <article class="portfolio-item pf-uielements pf-media clearfix">
                            <div class="portfolio-image">
                                <a href="#">
                                    <img src="/img/template1/yedekparca/yedek-parca-7.jpg" alt="Console Activity">
                                </a>
                                <div class="portfolio-overlay">
                                    <a href="/img/template1/yedekparca/yedek-parca-7.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
                                </div>
                            </div>
                            <div class="portfolio-desc">
                                <h3><a href="#">Güzine</a></h3>
                                <ul class="iconlist">
                                    <li><i class="icon-ok"></i> Sıcak iş çeliğinden imal edilmiştir.</li>
                                    <li><i class="icon-ok"></i> Yüksek çalışma sıcaklıklarına dayanıklı</li>
                                    <li><i class="icon-ok"></i> Darbeye ve eğilmeye dayanıklı</li>
                                    <li><i class="icon-ok"></i> Uzun ömürlü</li>
                                    <li><i class="icon-ok"></i> Dayanıklı dökümden imal edilmiştir.</li>
                                </ul>
                            </div>
                        </article>

                    </div><!-- #portfolio end -->

                </div>

            </div>

        </div>

    </div>

</section><!-- #content end -->

@endsection