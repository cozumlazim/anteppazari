@extends('layouts.template1.master')

@section('content')
<section id="slider" class="slider-parallax revslider-wrap clearfix">
    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

      <!-- Wrapper for slides -->
      <div class="carousel-inner" role="listbox">
        @foreach($sliders as $key => $slider)
            @if($key == "0")
                <div class="item active">
                  <img src="{{$slider->image}}" alt="{{$slider->title}}">
                </div>
            @else
                <div class="item">
                  <img src="{{$slider->image}}" alt="{{$slider->title}}">
                </div>
            @endif
        @endforeach
      </div>

      <!-- Controls -->
      <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
</section>
<!-- Content
============================================= -->
<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
            <!-- Post Content
            ============================================= -->
            <div class="postcontent nobottommargin col_last">
                <!-- Shop
                ============================================= -->
                <div id="shop" class="shop product-3 grid-container clearfix">,
                    @foreach($products as $product)
                    <?php
                    if($product->cat_title == "ANTEP FISTIĞI")
                    {
                        $alt_title = "ANTEP"; 
                    }
                    else
                    {
                        $alt_title = $product->cat_title;
                    }
                    ?>
                    <div class="product sf-{{$alt_title}} clearfix">
                        <div class="product-image">
                            <a href="/<?php echo str_slug($product->name); ?>"><img src="{{$product->image}}" alt="{{$product->name}}"></a>
                            <a href="/<?php echo str_slug($product->name); ?>"><img src="{{$product->second_image}}" alt="{{$product->name}}"></a>
                        </div>
                        <div class="product-desc center">
                            <div class="product-price"><h3 style="font-size: 18px;font-weight: 600;"><a href="/<?php echo str_slug($product->name); ?>">{{$product->name}}</a></h3></div>
                        </div>
                    </div>
                    @endforeach                    
                    </div><!-- #shop end -->
                    </div><!-- .postcontent end -->
                    <!-- Sidebar
                    ============================================= -->
                    <div class="sidebar nobottommargin">
                        <div class="sidebar-widgets-wrap">
                            <div class="widget widget-filter-links clearfix">
                                <h4><b>KATEGORİLER</b></h4>
                                <ul class="custom-filter" data-container="#shop" data-active-class="active-filter">
                                    <li class="widget-filter-reset active-filter"><a href="#" data-filter="*">temizle</a></li>
                                    <li><a href="#" data-filter=".sf-ANTEP">Antep Fıstığı</a></li>
                                    <li><a href="#" data-filter=".sf-BADEM">Badem</a></li>
                                    <li><a href="#" data-filter=".sf-CEVİZ">Ceviz</a></li>
                                    
                                    <li><a href="#" data-filter=".sf-KARIŞIK">Karışık Yemiş</a></li>
                                    
                                    <li><a href="#" data-filter=".sf-FINDIK">Fındık</a></li>
                                    
                                </ul>
                            </div>
                            <div class="widget widget-filter-links clearfix">
                                <h4><b>KURUMSAL</b></h4>
                                <ul>
                                    <li class="widget-filter-reset active-filter"><a href="#"></a></li>
                                     <li><a href="#">Biz Kimiz?</a></li>
                                    <li><a href="#">Teklif Alın</a></li>
                                    <li><a href="#">İletişim</a></li>
                                  
                                </ul>
                            </div>

                            <div class="widget widget-filter-links clearfix">
                                <h4><b>ZAMAN TÜNELİ</b></h4>
                                <ul>
                                    <li class="widget-filter-reset active-filter"><a href="#"></a></li>
                                    <li><a href="#">Fıstığın Tarihi</a></li>
                                    <li><a href="#">Bademin Tarihi</a></li>
                                    <li><a href="#">Cevizin Tarihi</a></li>
                                    <li><a href="#">Fındığın Tarihi</a></li>
                                </ul>
                            </div>


                        </div>
                        </div><!-- .sidebar end -->
                    </div>
                    
                    
                </section>
                <!-- #content end -->
                @endsection
@section('csscode')
<!-- SLIDER REVOLUTION 5.x CSS SETTINGS -->
<link rel="stylesheet" type="text/css" href="/include/rs-plugin/css/settings.css" media="screen" />
<link rel="stylesheet" type="text/css" href="/include/rs-plugin/css/layers.css">
<link rel="stylesheet" type="text/css" href="/include/rs-plugin/css/navigation.css">
<!-- Document Title
============================================= -->
<style>
.revo-slider-emphasis-text {
font-size: 64px;
font-weight: 700;
letter-spacing: -1px;
font-family: 'Raleway', sans-serif;
padding: 15px 20px;
border-top: 2px solid #FFF;
border-bottom: 2px solid #FFF;
}
.revo-slider-desc-text {
font-size: 20px;
font-family: 'Lato', sans-serif;
width: 650px;
text-align: center;
line-height: 1.5;
}
.revo-slider-caps-text {
font-size: 16px;
font-weight: 400;
letter-spacing: 3px;
font-family: 'Raleway', sans-serif;
}
.tp-video-play-button { display: none !important; }
.tp-caption { white-space: nowrap; }
</style>
@endsection
@section('jscode')
<!-- SLIDER REVOLUTION 5.x SCRIPTS  -->
<script type="text/javascript" src="/include/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="/include/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="/include/rs-plugin/js/extensions/revolution.extension.video.min.js"></script>
<script type="text/javascript" src="/include/rs-plugin/js/extensions/revolution.extension.slideanims.min.js"></script>
<script type="text/javascript" src="/include/rs-plugin/js/extensions/revolution.extension.actions.min.js"></script>
<script type="text/javascript" src="/include/rs-plugin/js/extensions/revolution.extension.layeranimation.min.js"></script>
<script type="text/javascript" src="/include/rs-plugin/js/extensions/revolution.extension.kenburn.min.js"></script>
<script type="text/javascript" src="/include/rs-plugin/js/extensions/revolution.extension.navigation.min.js"></script>
<script type="text/javascript" src="/include/rs-plugin/js/extensions/revolution.extension.migration.min.js"></script>
<script type="text/javascript" src="/include/rs-plugin/js/extensions/revolution.extension.parallax.min.js"></script>
<script type="text/javascript">
var tpj=jQuery;
tpj.noConflict();
tpj(document).ready(function() {
var apiRevoSlider = tpj('.tp-banner').show().revolution(
{
sliderType:"standard",
jsFileLocation:"include/rs-plugin/js/",
sliderLayout:"fullwidth",
dottedOverlay:"none",
delay:16000,
startwidth:1140,
startheight:600,
hideThumbs:200,
thumbWidth:100,
thumbHeight:50,
thumbAmount:5,
navigation: {
keyboardNavigation:"off",
keyboard_direction: "horizontal",
mouseScrollNavigation:"off",
onHoverStop:"off",
touch:{
touchenabled:"on",
swipe_threshold: 75,
swipe_min_touches: 1,
swipe_direction: "horizontal",
drag_block_vertical: false
},
arrows: {
style: "hermes",
enable: true,
hide_onmobile: false,
hide_onleave: false,
tmp: '<div class="tp-arr-allwrapper">   <div class="tp-arr-imgholder"></div>    <div class="tp-arr-titleholder">Selam</div> </div>',
left: {
h_align: "left",
v_align: "center",
h_offset: 10,
v_offset: 0
},
right: {
h_align: "right",
v_align: "center",
h_offset: 10,
v_offset: 0
}
}
},
touchenabled:"on",
onHoverStop:"on",
swipe_velocity: 0.7,
swipe_min_touches: 1,
swipe_max_touches: 1,
drag_block_vertical: false,
parallax:"mouse",
parallaxBgFreeze:"on",
parallaxLevels:[7,4,3,2,5,4,3,2,1,0],
keyboardNavigation:"off",
navigationHAlign:"center",
navigationVAlign:"bottom",
navigationHOffset:0,
navigationVOffset:20,
soloArrowLeftHalign:"left",
soloArrowLeftValign:"center",
soloArrowLeftHOffset:20,
soloArrowLeftVOffset:0,
soloArrowRightHalign:"right",
soloArrowRightValign:"center",
soloArrowRightHOffset:20,
soloArrowRightVOffset:0,
shadow:0,
fullWidth:"on",
fullScreen:"off",
spinner:"spinner4",
stopLoop:"off",
stopAfterLoops:-1,
stopAtSlide:-1,
shuffle:"off",
autoHeight:"off",
forceFullWidth:"off",
hideTimerBar:"on",
hideThumbsOnMobile:"off",
hideNavDelayOnMobile:1500,
hideBulletsOnMobile:"off",
hideArrowsOnMobile:"off",
hideThumbsUnderResolution:0,
hideSliderAtLimit:0,
hideCaptionAtLimit:0,
hideAllCaptionAtLilmit:0,
startWithSlide:0,
});
apiRevoSlider.bind("revolution.slide.onloaded",function (e) {
setTimeout( function(){ SEMICOLON.slider.sliderParallaxDimensions(); }, 200 );
});
apiRevoSlider.bind("revolution.slide.onchange",function (e,data) {
SEMICOLON.slider.revolutionSliderMenu();
});
});
</script>
@endsection