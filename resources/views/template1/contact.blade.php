@extends('layouts.template1.master')

@section('content')
	<!-- Page Title
    ============================================= -->
	<section id="page-title">
		<div class="singleSliderBack" style=""></div>

		<div class="container clearfix">
			<h1>İLETİŞİM</h1>
			<ol class="breadcrumb">
				<li><a href="/">Anasayfa</a></li>
				<li class="active">İletişim</li>
			</ol>
		</div>

	</section><!-- #page-title end -->

	<!-- Google Map
		============================================= 
	<section id="google-map" class="gmap"></section>-->

	<!-- Content
    ============================================= -->
    <section id="content">

		<div class="content-wrap">

			<div class="container clearfix">
				<!-- Contact Info
				============================================= -->
				<div class="row clear-bottommargin">

					<div class="col-md-3 col-sm-6 bottommargin clearfix">
						<div class="feature-box fbox-center fbox-bg fbox-plain contact-box-edit">
							<div class="fbox-icon">
								<a href="#"><i class="icon-map-marker2"></i></a>
							</div>
							<h3>Adres <hr><span class="subtitle">
							<address>
                            	    Nizip fıstıkçılar sitesi C blok No:13 Gaziantep<br>
                            	    <hr>
                                	Rami kuru gıda çarşısı 12.sokak No:58 Eyüp/İstanbul
                                <br>

                            </address>
						</div>
					</div>

					<div class="col-md-3 col-sm-6 bottommargin clearfix">
						<div class="feature-box fbox-center fbox-bg fbox-plain contact-box-edit">
							<div class="fbox-icon">
								<a href="#"><i class="icon-phone3"></i></a>
							</div>
							<h3>İRTİBAT BİLGİLERİ <hr>
							<span class="subtitle">
							0533 778 90 88
							<hr>
                            0212 497 43 40 41
                            <hr>
                            <strong>Fax:</strong> 0212 497 43 42<br></span></h3>
						</div>
					</div>

					<div class="col-md-3 col-sm-6 bottommargin clearfix">
						<div class="feature-box fbox-center fbox-bg fbox-plain contact-box-edit">
							<div class="fbox-icon">
								<a href="#"><i class="icon-email"></i></a>
							</div>
							<h3>E-Mail <hr><span class="subtitle">info@anteppazari.com.tr</span><hr><span class="subtitle">info@eforgida.com.tr</span></h3>
						</div>
					</div>

					<div class="col-md-3 col-sm-6 bottommargin clearfix">
						<div class="feature-box fbox-center fbox-bg fbox-plain contact-box-edit">
							<div class="fbox-icon">
								<a href="#"><i class="icon-phone-portrait"></i></a>
							</div>
							<h3>SOSYAL MEDYA 
								<hr>
								<a href="#" target="_blank"><span class="subtitle">Facebook</span></a>
								<hr>
								<a href="#" target="_blank"><span class="subtitle">Twitter</span></a>
								<hr>
								<a href="https://www.instagram.com/anteppazarioffical/" target="_blank"><span class="subtitle">Instagram</span></a>
							</h3>
						</div>
					</div>

				</div><!-- Contact Info End -->

				<div class="clear"></div>

				<!-- Contact Form
				============================================= -->
				<div class="col_half">

					<div class="fancy-title title-dotted-border">
						<h3>BİZE ULAŞIN</h3>
					</div>

					<div class="contact-widget">

					<div class="contact-form-result"></div>

						<div class="form-process"></div>

						<div class="col_one_third">
							<label for="template-contactform-name">Ad Soyad <small>*</small></label>
							<input type="text" id="bize-ulas-adsoyad" name="template-contactform-name" value="" class="sm-form-control required" />
						</div>

						<div class="col_one_third">
							<label for="template-contactform-email">Email <small>*</small></label>
							<input type="text" id="bize-ulas-email" name="template-contactform-email" value="" class="required email sm-form-control" />
						</div>

						<div class="col_one_third col_last">
							<label for="template-contactform-phone">Telefon</label>
							<input type="text" id="bize-ulas-telefon" name="template-contactform-phone" value="" class="sm-form-control" />
						</div>

						<div class="clear"></div>

						<div class="col_full">
							<label for="template-contactform-subject">Konu <small>*</small></label>
							<input type="text" id="bize-ulas-konu" name="template-contactform-subject" value="" class="required sm-form-control" />
						</div>
						

						<div class="clear"></div>

						<div class="col_full">
							<label for="template-contactform-message">Mesaj <small>*</small></label>
							<textarea class="required sm-form-control" id="bize-ulas-mesaj" name="template-contactform-message" rows="6" cols="30"></textarea>
						</div>

						<div class="col_full hidden">
							<input type="text" id="template-contactform-botcheck" name="template-contactform-botcheck" value="" class="sm-form-control" />
						</div>

						<div class="col_full">
							<button name="submit" type="submit" id="submit-button" tabindex="5" value="Submit" class="button button-3d nomargin" onclick="sendBizeUlas()">Gönder</button>
						</div>

					</div>

				</div><!-- Contact Form End -->

				<!-- Google Map
				============================================= -->
				<div class="col_half col_last">

					<section id="google-map" class="gmap" style="height: 524px;"></section>

				</div><!-- Google Map End -->

			</div>

		</div>

	</section><!-- #content end -->

@endsection
@section('jscode')
<script type="text/javascript">
function sendBizeUlas() {
  var ad = document.getElementById("bize-ulas-adsoyad").value;
  var telefon = document.getElementById("bize-ulas-telefon").value;
  var email = document.getElementById("bize-ulas-email").value;
  var konu = document.getElementById("bize-ulas-konu").value;
  var message = document.getElementById("bize-ulas-mesaj").value;
  if(ad == "")
  {
      alert("Lütfen 'Adınız' alanını doldurunuz.");
  }
  else if(telefon == "" || email == "")
  {
      alert("Lütfen 'Telefon' veya 'E-Mail' alanlarından birini doldurunuz.");
  }
  else
  {
      $.ajax({
          url: '/bize-ulasin-form',
          type: 'POST',
          beforeSend: function (xhr) {
              var token = $('meta[name="csrf_token"]').attr('content');

              if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
              }
          },
          cache: false,
          data: {ad: ad, telefon: telefon, email: email, konu: konu, mesaj: message},
          success: function(data){
              /*location.href = "/";*/
              alert("Bilgileriniz bize ulaşmıştır. En kısa sürede size dönüş sağlayacağız.")
              location.reload();
          },
          error: function(jqXHR, textStatus, err){}
      });
  }
}
</script>
@endsection