@extends('layouts.template1.master')

@section('content')
<!-- Page Title
    ============================================= -->
<section id="page-title">
    <div class="singleSliderBack"></div>

    <div class="container clearfix">
        <h1>{{$product->name}}</h1>
        <ol class="breadcrumb">
            <li><a href="/">ANASAYFA</a></li>
            <li><a href="/urunlerimiz">ÜRÜNLERİMİZ</a></li>
            <li class="active">{{$product->name}}</li>
        </ol>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">

            <div class="single-product">

                <div class="product">

                    <div class="col_two_fifth">

                        <!-- Product Single - Gallery
                        ============================================= -->
                        <div class="product-image">
                            <div class="fslider" data-pagi="false" data-arrows="false" data-thumbs="true">
                                <div class="flexslider">
                                    <div class="slider-wrap" data-lightbox="gallery">
                                        <div class="slide" data-thumb="{{$product->image}}"><a href="{{$product->image}}" title="{{$product->name}}" data-lightbox="gallery-item"><img src="{{$product->image}}" alt="{{$product->name}}"></a></div>
                                        <div class="slide" data-thumb="{{$product->second_image}}"><a href="{{$product->second_image}}" title="{{$product->name}}" data-lightbox="gallery-item"><img src="{{$product->second_image}}" alt="{{$product->name}}"></a></div>                                        
                                    </div>
                                </div>
                            </div>
                        </div><!-- Product Single - Gallery End -->

                    </div>

                    <div class="col_two_fifth product-desc">

                        <div class="clear"></div>
                        <div class="line"></div>

                        <!-- Product Single - Quantity & Cart Button
                        ============================================= -->
                        <form class="cart nobottommargin clearfix" method="post" enctype='multipart/form-data'>
                            <button type="submit" class="add-to-cart button nomargin">BİZE ULAŞIN</button>
                        </form><!-- Product Single - Quantity & Cart Button End -->

                        <div class="clear"></div>
                        <div class="line"></div>

                        <!-- Product Single - Short Description
                        ============================================= -->
                        <div style="padding-left:15px">
                        <?= $product->urun_bilgileri ?>
                        </div>
                        <!-- Product Single - Short Description End -->

                        <!-- Product Single - Meta
                        ============================================= -->
                        <div class="panel panel-default product-meta">
                            <div class="panel-body">
                                <span class="posted_in">Kategori: <a href="#" rel="tag">{{$categories->title}}</a>.</span>
                                <span class="tagged_as">Etiket: <a href="#" rel="tag">{{$product->tags}}</a></span>
                            </div>
                        </div><!-- Product Single - Meta End -->

                    </div>

                    <div class="col_full nobottommargin">

                        <div class="tabs clearfix nobottommargin" id="tab-1">

                            <ul class="tab-nav clearfix">
                                <li><a href="#tabs-1"><i class="icon-info-sign"></i><span class="hidden-xs"> Kullanım Alanları</span></a></li>
                                <li><a href="#tabs-2"><i class="icon-warning-sign"></i><span class="hidden-xs"> Faydaları</span></a></li>
                                <li><a href="#tabs-3"><i class="icon-book3"></i><span class="hidden-xs"> Tarihçe</span></a></li>
                            </ul>

                            <div class="tab-container">

                                <div class="tab-content clearfix" id="tabs-1">

                                    <div id="portfolio" class="portfolio grid-container clearfix">
                                    @foreach($usefor as $value)
                                        <article class="portfolio-item pf-media pf-icons">
                                            <div class="portfolio-image">
                                                <a href="portfolio-single.html">
                                                    <img src="{{$value->image}}" alt="{{$value->title}}">
                                                </a>
                                                <div class="portfolio-overlay">
                                                    <a href="{{$value->image}}" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
                                                </div>
                                            </div>
                                            <div class="portfolio-desc">
                                                <h3 style="text-align: center;font-style: italic; font-weight: bold;"><a href="#">{{$value->title}}</a></h3>
                                            </div>
                                        </article>
                                    @endforeach
                                    </div><!-- #portfolio end -->
                                </div>
                                <div class="tab-content clearfix" id="tabs-2" style="padding-left:30px;">
                                    <?= $product->fayda_content ?>
                                </div>
                                <div class="tab-content clearfix" id="tabs-3">
                                    <?= $product->tarih_content ?>
                                </div>
                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section><!-- #content end -->

@endsection