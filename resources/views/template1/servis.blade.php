@extends('layouts.template1.master')

@section('content')
<!-- Page Title
    ============================================= -->
<section id="page-title">
    <div class="singleSliderBack" style=""></div>


    <div class="container clearfix">
        <h1>Teklif Al</h1>
        <ol class="breadcrumb">
            <li><a href="/">Anasayfa</a></li>
            <li class="active">Teklif Al</li>
        </ol>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">

            <!-- Post Content
					============================================= -->
            <div class="col_full">
                <h3>TEKLİF AL FORMU</h3>
                <div class="contact-widget">

                    <div class="contact-form-result"></div>

                    <div class="form-process"></div>

                    <div class="col_one_third">
                        <label for="template-contactform-name">Ad Soyad <small>*</small></label>
                        <input type="text" id="teklif-al-adsoyad" name="template-contactform-name" value="" class="sm-form-control required" />
                    </div>

                    <div class="col_one_third">
                        <label for="template-contactform-email">Email <small>*</small></label>
                        <input type="email" id="teklif-al-email" name="template-contactform-email" value="" class="required email sm-form-control" />
                    </div>

                    <div class="col_one_third col_last">
                        <label for="template-contactform-phone">Telefon</label>
                        <input type="text" id="teklif-al-telefon" name="template-contactform-phone" value="" class="sm-form-control" />
                    </div>

                    <div class="clear"></div>

                    <div class="col_two_third">
                        <label for="template-contactform-service">Ürün</label>
                        <select id="teklif-al-urun" name="template-contactform-service" class="sm-form-control">
                            <option value="">-- Seçiniz --</option>
                            @foreach($product as $value)
                            <option >{{ $value->name }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="clear"></div>

                    <div class="col_full">
                        <label for="template-contactform-message">Mesaj <small>*</small></label>
                        <textarea class="required sm-form-control" id="teklif-al-mesaj" name="template-contactform-message" rows="6" cols="30"></textarea>
                    </div>
                    
                    <div class="col_full">
                        <button class="button button-3d nomargin" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit" onclick="sendTeklifAl()">Gönder</button>
                    </div>
                </div>

            </div><!-- .postcontent end -->

            <div class="clear"></div>

            <!--<div class="col_full">

                <div class="heading-block center nobottomborder">
                    <h2>VAROL MAKİNE METAL ENJEKSİYON, YEDEK PARÇA VE REVİZYON SANAYİ</h2>
                    <span>Uzman ekibimizle sizlere yardımcı olmaktan mutluluk duyuyoruz.</span>
                </div>

                <div class="promo promo-light bottommargin promo-edit">
                    <span class="promo-edit-span" style="text-align:justify;">
                        <p>Makineniz için ihtiyacınız olan herşeyi sağlıyoruz. Kendi alanında uzman servis mühendislerimiz size memnuniyetle yardımcı olacaktır. İlk aşamada uzman mühendislerimiz arızanın uzaktan erişim ile çözülebilir olup olmadığına karar verir. Uzaktan erişim ile giderilecek her türlü arıza zaman kaybınızı minimize ederken rakiplerinize karşı sizi avantajlı kılar.
                        </p>
                        <ul>
                            <li>Hızlı ve güvenilir servis desteği.</li>
                            <li>İyi eğitimli servis mühendislerimiz.</li>
                            <li>Kusursuz hizmet anlayışı.</li>
                        </ul>
                    </span>
                </div>

            </div>-->

        </div>

    </div>

</section><!-- #content end -->

@endsection
@section('jscode')
<script type="text/javascript">
function sendTeklifAl() {
  var ad = document.getElementById("teklif-al-adsoyad").value;
  var telefon = document.getElementById("teklif-al-telefon").value;
  var email = document.getElementById("teklif-al-email").value;
  var urun = document.getElementById("teklif-al-urun").value;
  var message = document.getElementById("teklif-al-mesaj").value;
  if(ad == "")
  {
      alert("Lütfen 'Adınız' alanını doldurunuz.");
  }
  else if(telefon == "" || email == "")
  {
      alert("Lütfen 'Telefon' veya 'E-Mail' alanlarından birini doldurunuz.");
  }
  else
  {
      $.ajax({
          url: '/teklif-al-form',
          type: 'POST',
          beforeSend: function (xhr) {
              var token = $('meta[name="csrf_token"]').attr('content');

              if (token) {
                  return xhr.setRequestHeader('X-CSRF-TOKEN', token);
              }
          },
          cache: false,
          data: {ad: ad, telefon: telefon, email: email, urun: urun, mesaj: message},
          success: function(data){
              /*location.href = "/";*/
              alert("Bilgileriniz bize ulaşmıştır. En kısa sürede size dönüş sağlayacağız.")
              location.reload();
          },
          error: function(jqXHR, textStatus, err){}
      });
  }
}
</script>
@endsection