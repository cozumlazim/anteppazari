@extends('layouts.template0.master')

@section('content')


<div role="main" class="main">

				<section class="page-header">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<ul class="breadcrumb">
									<li><a href="/">Anasayfa</a></li>
									<li class="active">Neler Yapıyoruz?</li>
								</ul>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<h1>Neler Yapıyoruz?</h1>
							</div>
						</div>
					</div>
				</section>

				<div class="container">

					<div class="row">
						<div class="col-md-12">
							<h2 class="word-rotator-title">
								Haliç Yapı <strong>
                                    <span class="word-rotate" data-plugin-options='{"delay": 2000}'>
                                        <span class="word-rotate-items">
                                            <span>Neler Yapıyor?</span>
                                            <span>İzolasyon</span>
                                            <span>Seramik Uygulamalar</span>
                                            <span>Taş Kaplama</span>
                                            <span>Boya İşleri</span>
                                            <span>Parke Uygulamaları</span>
                                            <span>Çatı İşlemleri</span>
                                            <span>Şap Uygulamaları</span>
                                            <span>Alçıpan Uygulamaları</span>
                                            <span>Mobilya Uygulamaları</span>
                                            <span>Dış Cephe Uygulamaları</span>
                                            <span>PVC Sistemleri</span>
                                        </span>
                                    </span>
								</strong>
							</h2>
						</div>
					</div>

					<div class="row">
						<div class="col-md-10">
							<p class="lead">
								<?=$pageService[0]['content']?>
							</p>
						</div>
						<!--<div class="col-md-2">
							<a href="/iletisim" class="btn btn-lg btn-primary mt-xl pull-right">Gelin İşinizin Mimarı Olalım!</a>
						</div>-->
					</div>

					<hr>

					<div class="featured-boxes">
						<div class="row">
							<?php foreach ($services as $key => $service): ?>						
							<div class="col-md-3 col-sm-6">
								<div class="featured-box featured-box-primary featured-box-effect-1 mt-xlg">
									<div class="box-content">
										@if($service->title == "Taş Kaplama")
											<img src="/img/neler-yapiyoruz/taskaplama.png" style="height:38px;">
										@elseif($service->title == "Seramik Uygulamalar")
											<img src="/img/neler-yapiyoruz/seramikuygulamalar.png" style="height:38px;">
										@elseif($service->title == "Alçıpan Uygulamaları")
											<img src="/img/neler-yapiyoruz/alcipanuygulamalari.png" style="height:38px;">
										@elseif($service->title == "Şap Uygulamaları")
											<img src="/img/neler-yapiyoruz/sapuygulamalari.png" style="height:38px;">
										@elseif($service->title == "Parke Uygulamaları")
											<img src="/img/neler-yapiyoruz/parkeuygulamalari.png" style="height:38px;">
										@elseif($service->title == "Çatı İşlemleri")
											<img src="/img/neler-yapiyoruz/cati-islemleri.png" style="height:38px;">
										@elseif($service->title == "Boya İşleri")
											<img src="/img/neler-yapiyoruz/boyama.png" style="height:38px;">
										@elseif($service->title == "İzolasyon")
											<img src="/img/neler-yapiyoruz/izolasyon.png" style="height:38px;">
										@elseif($service->title == "Mobilya Uygulamaları")
											<img src="/img/neler-yapiyoruz/dolap.png" style="height:38px;">
										@elseif($service->title == "Dış Cephe Uygulamaları")
											<img src="/img/neler-yapiyoruz/mantolama.jpg" style="height:38px;">
										@elseif($service->title == "PVC Sistemleri")
											<img src="/img/neler-yapiyoruz/pvc.png" style="height:38px;">
										@else
											<i class="fa fa-star-o"></i>
										@endif
										<h4 class="text-uppercase">{{$service->title}}</h4>
										<p>{{$service->short_content}}</p>
										<!--<p><a href="{{$service->slug}}/{{$service->id}}" class="lnk-primary learn-more">İncele <i class="fa fa-angle-right"></i></a></p>-->
									</div>
								</div>
							</div>
							<?php endforeach ?>
						</div>
					</div>

					<hr>
				</div>

			</div>


@endsection