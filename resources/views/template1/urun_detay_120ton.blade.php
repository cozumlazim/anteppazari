@extends('layouts.template1.master')

@section('content')
<!-- Page Title
    ============================================= -->
<section id="page-title">
    <div class="singleSliderBack" style="background-image: url('/img/template1/cover.jpg');"></div>

    <div class="container clearfix">
        <h1>120 ton Sıcak Kamara Metal Enjeksiyon</h1>
        <span>120 ton Sıcak Kamara Metal Enjeksiyon bilgilerine aşağıdan ulaşabilirsiniz.</span>
        <ol class="breadcrumb">
            <li><a href="/">Anasayfa</a></li>
            <li>Metal Enjeksiyon</li>
            <li class="active">120 ton Sıcak Kamara Metal Enjeksiyon</li>
        </ol>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">

            <div class="single-product">

                <div class="product">

                    <div class="col_two_fifth">

                        <!-- Product Single - Gallery
                        ============================================= -->
                        <div class="product-image">
                            <div class="fslider" data-pagi="false" data-arrows="false" data-thumbs="true">
                                <div class="flexslider">
                                    <div class="slider-wrap" data-lightbox="gallery">
                                        <div class="slide" data-thumb="/img/template1/30-ton/arka-1.png"><a href="/img/template1/30-ton/arka-1.png" title="120 ton Sıcak Kamara Metal Enjeksiyon" data-lightbox="gallery-item"><img src="/img/template1/30-ton/arka-1.png" alt="120 ton Sıcak Kamara Metal Enjeksiyon"></a></div>
                                        <div class="slide" data-thumb="/img/template1/30-ton/arka-2.png"><a href="/img/template1/30-ton/arka-2.png" title="120 ton Sıcak Kamara Metal Enjeksiyon" data-lightbox="gallery-item"><img src="/img/template1/30-ton/arka-2.png" alt="120 ton Sıcak Kamara Metal Enjeksiyon"></a></div>
                                        <div class="slide" data-thumb="/img/template1/30-ton/foto1.png"><a href="/img/template1/30-ton/foto1.png" title="120 ton Sıcak Kamara Metal Enjeksiyon" data-lightbox="gallery-item"><img src="/img/template1/30-ton/foto1.png" alt="120 ton Sıcak Kamara Metal Enjeksiyon"></a></div>
                                        <div class="slide" data-thumb="/img/template1/30-ton/foto2.png"><a href="/img/template1/30-ton/foto2.png" title="120 ton Sıcak Kamara Metal Enjeksiyon" data-lightbox="gallery-item"><img src="/img/template1/30-ton/foto2.png" alt="120 ton Sıcak Kamara Metal Enjeksiyon"></a></div>
                                        <div class="slide" data-thumb="/img/template1/30-ton/on-1.png"><a href="/img/template1/30-ton/on-1.png" title="120 ton Sıcak Kamara Metal Enjeksiyon" data-lightbox="gallery-item"><img src="/img/template1/30-ton/on-1.png" alt="120 ton Sıcak Kamara Metal Enjeksiyon"></a></div>
                                        <div class="slide" data-thumb="/img/template1/30-ton/sag-3.png"><a href="/img/template1/30-ton/sag-3.png" title="120 ton Sıcak Kamara Metal Enjeksiyon" data-lightbox="gallery-item"><img src="/img/template1/30-ton/sag-3.png" alt="120 ton Sıcak Kamara Metal Enjeksiyon"></a></div>
                                        <div class="slide" data-thumb="/img/template1/30-ton/sag-yan-1.png"><a href="/img/template1/30-ton/sag-yan-1.png" title="120 ton Sıcak Kamara Metal Enjeksiyon" data-lightbox="gallery-item"><img src="/img/template1/30-ton/sag-yan-1.png" alt="120 ton Sıcak Kamara Metal Enjeksiyon"></a></div>
                                        <div class="slide" data-thumb="/img/template1/30-ton/sol-0.png"><a href="/img/template1/30-ton/sol-0.png" title="120 ton Sıcak Kamara Metal Enjeksiyon" data-lightbox="gallery-item"><img src="/img/template1/30-ton/sol-0.png" alt="120 ton Sıcak Kamara Metal Enjeksiyon"></a></div>
                                        <div class="slide" data-thumb="/img/template1/30-ton/sol-yan-0.png"><a href="/img/template1/30-ton/sol-yan-0.png" title="120 ton Sıcak Kamara Metal Enjeksiyon" data-lightbox="gallery-item"><img src="/img/template1/30-ton/sol-yan-0.png" alt="120 ton Sıcak Kamara Metal Enjeksiyon"></a></div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- Product Single - Gallery End -->

                    </div>

                    <div class="col_two_fifth product-desc">

                        <div class="clear"></div>
                        <div class="line"></div>

                        <!-- Product Single - Quantity & Cart Button
                        ============================================= -->
                        <form class="cart nobottommargin clearfix" method="post" enctype='multipart/form-data'>
                            <button type="submit" class="add-to-cart button nomargin">BİZE ULAŞIN</button>
                        </form><!-- Product Single - Quantity & Cart Button End -->

                        <div class="clear"></div>
                        <div class="line"></div>

                        <!-- Product Single - Short Description
                        ============================================= -->
                        <ul class="iconlist">
                            <li><i class="icon-caret-right"></i> 120 Ton</li>
                            <li><i class="icon-caret-right"></i> 10 Ton enjeksiyon kuvveti</li>
                            <li><i class="icon-caret-right"></i> 21 galon/dk Hidrolik pompa</li>
                            <li><i class="icon-caret-right"></i> 3.000 - 3.500 Günlük baskı adeti</li>
                            <li><i class="icon-caret-right"></i> Plc ve dokunmatik ekran kolaylığı</li>
                        </ul><!-- Product Single - Short Description End -->

                        <!-- Product Single - Meta
                        ============================================= -->
                        <div class="panel panel-default product-meta">
                            <div class="panel-body">
                                <span class="posted_in">Kategori: <a href="#" rel="tag">Metal Enjeksiyon</a>.</span>
                                <span class="tagged_as">Etiket: <a href="#" rel="tag">Metal Enjeksiyon</a>, <a href="#" rel="tag">Varol Makine</a>, <a href="#" rel="tag">120 ton Sıcak Kamara Metal Enjeksiyon</a></span>
                            </div>
                        </div><!-- Product Single - Meta End -->

                    </div>

                    <div class="col_full nobottommargin">

                        <div class="tabs clearfix nobottommargin" id="tab-1">

                            <ul class="tab-nav clearfix">
                                <li><a href="#tabs-1"><i class="icon-info-sign"></i><span class="hidden-xs"> Teknik Özellikler</span></a></li>
                                <li><a href="#tabs-2"><i class="icon-warning-sign"></i><span class="hidden-xs"> Güvenlik Uyarılar</span></a></li>
                                <li><a href="#tabs-3"><i class="icon-book3"></i><span class="hidden-xs"> Bakım</span></a></li>
                            </ul>

                            <div class="tab-container">

                                <div class="tab-content clearfix" id="tabs-1">

                                    <table class="table table-striped table-bordered">
                                        <tbody>
                                        <tr>
                                            <td>Mengene Kilitleme Kuvveti</td>
                                            <td>120 Ton</td>
                                        </tr>
                                        <tr>
                                            <td>Kolon Arası Mesafe</td>
                                            <td>370x370 mm</td>
                                        </tr>
                                        <tr>
                                            <td>Gezer Plaka Hareket Mesafesi</td>
                                            <td>270 mm</td>
                                        </tr>
                                        <tr>
                                            <td>Enjeksiyon Kuvveti</td>
                                            <td>10 Ton</td>
                                        </tr>
                                        <tr>
                                            <td>Enjeksiyon Silindiri Strogu</td>
                                            <td>150 mm</td>
                                        </tr>
                                        <tr>
                                            <td>Hidrolik Pompa</td>
                                            <td>21 galon/dk</td>
                                        </tr>
                                        <tr>
                                            <td>Motor</td>
                                            <td>18 kw/sa</td>
                                        </tr>
                                        <tr>
                                            <td>Tank Yağ Kapasitesi</td>
                                            <td>350 Lt 68 Numara Hidrolik Yağ</td>
                                        </tr>
                                        <tr>
                                            <td>Toplam Elektrik</td>
                                            <td>43 kw</td>
                                        </tr>
                                        <tr>
                                            <td>Günlük Baskı Adeti</td>
                                            <td>3.000 - 3.500</td>
                                        </tr>
                                        <tr>
                                            <td>Elektironik Sistemi</td>
                                            <td>Plc ve dokunmatik ekran kolaylığı</td>
                                        </tr>
                                        <tr>
                                            <td>Kalıp Koruma Sistemi</td>
                                            <td>Terazili kalıp koruma sistem güvenliği, kalıpta ürün kaldığında makineyi durdurur.</td>
                                        </tr>
                                        </tbody>
                                    </table>

                                </div>
                                <div class="tab-content clearfix" id="tabs-2">
                                    <ul>
                                        <li>Çalışan operatörün makina hakkında çalışıcak kadar bilgili olması.</li>
                                        <li>Makinanın koruyucu kapakları çalışırken kesinlikle kapalı olmalıdır.</li>
                                        <li>Makinanın koruyucu kapaklarında sivic sistemi vardır. Açıkken çalışmaz fakat operatör tarafından müdahale edilip devre dışı bırakılabilir. İşveren olarak kesinlikle müsade etmeyiniz. Aksi takdirde doğabilecek bir iş kazasından firmamız sorumlu değildir.</li>
                                        <li>Temizlik ve bakım yapılırken makinayı stopediniz.</li>
                                        <li>Makinada ani basınç boşaltıcı sistemimiz vardır. Hidrolik sistemde olabilecek ani yağ kaçağında makina stop edilerek bütün hidrolik basınç boşalır.</li>
                                        <li>Hidrolik arink bakımı veya valf sökme gibi işlemler yapmadan makina stop edilip motorun yanındaki nanometreler sıfırı göstermelidir. Sonra işleme başlaybilirsiniz.</li>
                                        <li>Pataya zamak atarken koruyucu eldiven ve gözlük takınız.</li>
                                        <li>Kalıp bağlarken azami dikkat gösteriniz.</li>
                                    </ul>
                                </div>
                                <div class="tab-content clearfix" id="tabs-3">
                                    <div class="tabs side-tabs responsive-tabs clearfix" id="tab-4">
                                        <ul class="tab-nav clearfix">
                                            <li><a href="#tabs-13">Günlük Bakım</a></li>
                                            <li><a href="#tabs-14">Aylık Bakım</a></li>
                                            <li><a href="#tabs-15">Yıllık Bakım</a></li>
                                        </ul>

                                        <div class="tab-container">

                                            <div class="tab-content clearfix" id="tabs-13">
                                                <ul>
                                                    <li>Makinayı çalıştırmadan makinanın etrafını bir dolaşın, herhangi bir yağ kaçağı varmı kontrol ediniz.</li>
                                                    <li>Zamagı eridiğinden emin olunuz. Ekrandan gata ve meme ışılarını kontrol ediniz.(450 - 500)</li>
                                                    <li>Makina kızaklarını silip, kızak yağıyla yağlayınız.</li>
                                                    <li>Makinada sabahleyin kalıp civatalarını mutlaka kontrol ediniz. Isınıp soğuduğu için civatalarda gevşeme olabilir.</li>
                                                    <li>Gurup dayandı sivicini kontrol ediniz.</li>
                                                    <li>Makinaya start verdiğinizde makas yağlama çalışıyormu, yağlama hortumlarını kontrol ediniz.</li>
                                                </ul>
                                            </div>
                                            <div class="tab-content clearfix" id="tabs-14">
                                                <ul>
                                                    <li>Günlük bakımın aynısı tekrarlanır.</li>
                                                    <li>Makina komple temizlenir, hidrolik yağ kaçağı varmı kontrol edilir.</li>
                                                    <li>Potanın etrafındaki curuflar temizlenir.</li>
                                                    <li>Piston ve sekmanları kontrol edilir. Aşınmışsa değiştirilir.</li>
                                                    <li>Makinanın soğutma hortumları kontrol ediniz. Ön enjeksiyon kafasında suyun dolaştığından emin olunuz. Kireçlenip tıkanmışsa açınız.</li>
                                                    <li>Makas yağlama motorunun yağ seviyesini kontrol ediniz, eksikse tamamlayınız. 3 ayda bir değiştiriniz.</li>
                                                </ul>
                                            </div>
                                            <div class="tab-content clearfix" id="tabs-15">
                                                <ul>
                                                    <li>Makinanın enjeksiyon gurup, itici ve mengene  oringleri değiştirilir.</li>
                                                    <li>Oring değişiminden sonra basınçlar kontrol edilir.</li>
                                                    <li>Günlük ve aylık bakımlar tekrarlanır.</li>
                                                    <li>Yıllık bakım için gerekli bilgiye sahipseniz işlemleri yapınız veya firmamızdan teknik servis alabilirsiniz.</li>
                                                    <li>Firmamızla irtibat kurarak değişimleri yapabilirsiniz.</li>
                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</section><!-- #content end -->

@endsection