@extends('layouts.template1.master')

@section('content')
<!-- Page Title
    ============================================= -->
<section id="page-title">
    <div class="singleSliderBack" style="background-image: url('/img/template1/cover.jpg');"></div>

    <div class="container clearfix">
        <h1>İkinci El</h1>
        <span>İkinci el makinalar hakkında detaylı bilgiye buradan ulaşabilirsiniz.</span>
        <ol class="breadcrumb">
            <li><a href="#">Anasayfa</a></li>
            <li class="active">İkinci El</li>
        </ol>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">

    <div class="">

        <div class="container clearfix">

            <div class="col_full">

                <div class="fslider" data-pagi="false" data-animation="fade">
                    <div class="flexslider">
                        <div class="slider-wrap">
                            <div class="slide"><a href="#"><img src="/img/template1/ikinci-el.jpg" alt="İkinci El"></a></div>
                        </div>
                    </div>
                </div>

                <div class="heading-block center nobottomborder">
                    <h2>VAROL MAKİNE METAL ENJEKSİYON, YEDEK PARÇA VE REVİZYON SANAYİ</h2>
                    <span>Uzman ekibimizle sizlere yardımcı olmaktan mutluluk duyuyoruz.</span>
                </div>

            </div>

            <div class="clear"></div>


        </div>

    </div>

</section><!-- #content end -->

@endsection