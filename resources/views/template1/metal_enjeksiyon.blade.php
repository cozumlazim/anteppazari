@extends('layouts.template1.master')

@section('content')
<!-- Page Title
    ============================================= -->
<section id="page-title">
    <div class="singleSliderBack" style="background-image: url('/img/template1/cover.jpg');"></div>

    <div class="container clearfix">
        <h1>Metal Enjeksiyon</h1>
        <span>Metal Enjeksiyon ürünlerimize aşağıdan ulaşabilirsiniz.</span>
        <ol class="breadcrumb">
            <li><a href="/">Anasayfa</a></li>
            <li class="active">Metal Enjeksiyon</li>
        </ol>
    </div>

</section><!-- #page-title end -->

<!-- Content
============================================= -->
<section id="content">

    <div class="content-wrap">

        <div class="container clearfix">

            <!-- Post Content
					============================================= -->
            <div class="postcontent nobottommargin col_last">

                <!-- Shop
                ============================================= -->
                <div id="shop" class="shop product-3 grid-container clearfix">

                    <div class="product sf-30ton clearfix">
                        <div class="product-image">
                            <a href="#"><img src="/img/template1/30-ton/arka-0.png" alt="30 ton Sıcak Kamara Metal Enjeksiyon Makinesi"></a>
                            <a href="#"><img src="/img/template1/30-ton/arka-1.png" alt="30 ton Sıcak Kamara Metal Enjeksiyon Makinesi"></a>
                            <div class="product-overlay">
                                <a href="/metal-enjeksiyon/30-ton-sicak-kamara-metal-enjeksiyon-makinesi" class="add-to-cart"><i class="icon-shopping-cart"></i><span> Detay</span></a>
                                <a href="/include/ajax/shop-item-30ton.html" class="item-quick-view" data-lightbox="ajax"><i class="icon-zoom-in2"></i><span> Hızlı Görüntüle</span></a>
                            </div>
                        </div>
                        <div class="product-desc center">
                            <div class="product-title"><h3><a href="/metal-enjeksiyon/30-ton-sicak-kamara-metal-enjeksiyon-makinesi">30 ton Sıcak Kamara Metal Enjeksiyon Makinesi</a></h3></div>
                        </div>
                    </div>

                    <div class="product sf-120ton clearfix">
                        <div class="product-image">
                            <a href="#"><img src="/img/template1/30-ton/on-3.png" alt="120 ton Sıcak Kamara Metal Enjeksiyon Makinesi"></a>
                            <a href="#"><img src="/img/template1/30-ton/on-2.png" alt="120 ton Sıcak Kamara Metal Enjeksiyon Makinesi"></a>
                            <div class="product-overlay">
                                <a href="/metal-enjeksiyon/120-ton-sicak-kamara-metal-enjeksiyon-makinesi" class="add-to-cart"><i class="icon-shopping-cart"></i><span> Detay</span></a>
                                <a href="/include/ajax/shop-item-120ton.html" class="item-quick-view" data-lightbox="ajax"><i class="icon-zoom-in2"></i><span> Hızlı Görüntüle</span></a>
                            </div>
                        </div>
                        <div class="product-desc center">
                            <div class="product-title"><h3><a href="#">120 ton Sıcak Kamara Metal Enjeksiyon Makinesi</a></h3></div>
                        </div>
                    </div>

                    <div class="product sf-200ton clearfix">
                        <div class="product-image">
                            <a href="#"><img src="/img/template1/400-ton/400-ton-3.png" alt="200 ton Sıcak Kamara Metal Enjeksiyon Makinesi"></a>
                            <a href="#"><img src="/img/template1/400-ton/400-ton-2.png" alt="200 ton Sıcak Kamara Metal Enjeksiyon Makinesi"></a>
                            <div class="product-overlay">
                                <a href="/metal-enjeksiyon/200-ton-sicak-kamara-metal-enjeksiyon-makinesi" class="add-to-cart"><i class="icon-shopping-cart"></i><span> Detay</span></a>
                                <a href="/include/ajax/shop-item-200ton.html" class="item-quick-view" data-lightbox="ajax"><i class="icon-zoom-in2"></i><span> Hızlı Görüntüle</span></a>
                            </div>
                        </div>
                        <div class="product-desc center">
                            <div class="product-title"><h3><a href="#">200 ton Sıcak Kamara Metal Enjeksiyon Makinesi</a></h3></div>
                        </div>
                    </div>

                    <div class="product sf-400ton clearfix">
                        <div class="product-image">
                            <a href="#"><img src="/img/template1/400-ton/400-ton-1.png" alt="Light Blue Denim Dress"></a>
                            <a href="#"><img src="/img/template1/400-ton/400-ton-2.png" alt="Light Blue Denim Dress"></a>
                            <div class="product-overlay">
                                <a href="/metal-enjeksiyon/400-ton-sicak-kamara-metal-enjeksiyon-makinesi" class="add-to-cart"><i class="icon-shopping-cart"></i><span> Detay</span></a>
                                <a href="/include/ajax/shop-item-400ton.html" class="item-quick-view" data-lightbox="ajax"><i class="icon-zoom-in2"></i><span> Hızlı Görüntüle</span></a>
                            </div>
                        </div>
                        <div class="product-desc center">
                            <div class="product-title"><h3><a href="#">400 ton Sıcak Kamara Metal Enjeksiyon Makinesi</a></h3></div>
                        </div>
                    </div>

                </div><!-- #shop end -->

            </div><!-- .postcontent end -->
            <div class="clear"></div>

            <div class="col_full">

                <div class="heading-block center nobottomborder">
                    <h2>VAROL MAKİNE METAL ENJEKSİYON, YEDEK PARÇA VE REVİZYON SANAYİ</h2>
                    <span>Uzman ekibimizle sizlere yardımcı olmaktan mutluluk duyuyoruz.</span>
                </div>

                <div class="promo promo-light bottommargin promo-edit">
                    <span class="promo-edit-span" style="text-align:justify;">
                        <p>Her marka model halat örme makineleri beşik, şansımaz, yataklama ve kaporta gibi komple bakım ve revizyon işlerini yapmaktayız
                        </p>
                        <p>
                        Eski, yıpranmış ve performans kaybına uğramış makineleri yeni teknoloji ile tanıştırıyoruz. Yıpranan kısımlarının tadilatını yaparak, performansını artıracak işlemler yapıyoruz.
                        </p>
                        <p>
                        Makine revizyonu konusunda resimlerde gördüğünüz örneklerden de anlaşılacağı gibi önemli referans noktalarımız bulunmaktadır.
                        </p>
                    </span>
                </div>

            </div>

            <div class="clear"></div>

            <div class="container clearfix" style="margin-top: 80px;">

                <div class="heading-block center">
                    <h3>Yedek Parça</h3>
                    <span>Yaptığımız bazı projelere aşağıdan ulaşabilirsiniz.</span>
                </div>

                <div id="oc-portfolio" class="owl-carousel portfolio-carousel portfolio-nomargin carousel-widget owl-edit" data-margin="1" data-pagi="false" data-autoplay="5000" data-items-xxs="1" data-items-xs="2" data-items-sm="3" data-items-lg="4">

                    <div class="oc-item">
                        <div class="iportfolio">
                            <div class="portfolio-image">
                                <a href="/yedek-parca">
                                    <img src="/img/template1/yedekparca/yedek-parca-1k.jpg" alt="Open Imagination">
                                </a>
                                <div class="portfolio-overlay">
                                    <a href="/img/template1/yedekparca/yedek-parca-1.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
                                    <a href="/yedek-parca" class="right-icon"><i class="icon-line-ellipsis"></i></a>
                                </div>
                            </div>
                            <div class="portfolio-desc">
                                <h3><a href="/yedek-parca">Ara Boru</a></h3>
                            </div>
                        </div>
                    </div>

                    <div class="oc-item">
                        <div class="iportfolio">
                            <div class="portfolio-image">
                                <a href="/yedek-parca">
                                    <img src="/img/template1/yedekparca/yedek-parca-2k.jpg" alt="Locked Steel Gate">
                                </a>
                                <div class="portfolio-overlay">
                                    <a href="/img/template1/yedekparca/yedek-parca-2.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
                                    <a href="/yedek-parca" class="right-icon"><i class="icon-line-ellipsis"></i></a>
                                </div>
                            </div>
                            <div class="portfolio-desc">
                                <h3><a href="/yedek-parca">Piston</a></h3>
                            </div>
                        </div>
                    </div>

                    <div class="oc-item">
                        <div class="iportfolio">
                            <div class="portfolio-image">
                                <a href="#">
                                    <img src="/img/template1/yedekparca/yedek-parca-3k.jpg" alt="Mac Sunglasses">
                                </a>
                                <div class="portfolio-overlay">
                                    <a href="/img/template1/yedekparca/yedek-parca-3.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
                                    <a href="portfolio-single-video.html" class="right-icon"><i class="icon-line-ellipsis"></i></a>
                                </div>
                            </div>
                            <div class="portfolio-desc">
                                <h3><a href="portfolio-single-video.html">Nuzzle</a></h3>
                            </div>
                        </div>
                    </div>

                    <div class="oc-item">
                        <div class="iportfolio">
                            <div class="portfolio-image">
                                <a href="/yedek-parca">
                                    <img src="/img/template1/yedekparca/yedek-parca-4k.jpg" alt="Console Activity">
                                </a>
                                <div class="portfolio-overlay">
                                    <a href="/img/template1/yedekparca/yedek-parca-4.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
                                    <a href="/yedek-parca" class="right-icon"><i class="icon-line-ellipsis"></i></a>
                                </div>
                            </div>
                            <div class="portfolio-desc">
                                <h3><a href="/yedek-parca">Piston - Segman - Nuzzle</a></h3>
                            </div>
                        </div>
                    </div>

                    <div class="oc-item">
                        <div class="iportfolio">
                            <div class="portfolio-image">
                                <a href="/yedek-parca">
                                    <img src="/img/template1/yedekparca/yedek-parca-5k.jpg" alt="Console Activity">
                                </a>
                                <div class="portfolio-overlay">
                                    <a href="/img/template1/yedekparca/yedek-parca-5.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
                                    <a href="/yedek-parca" class="right-icon"><i class="icon-line-ellipsis"></i></a>
                                </div>
                            </div>
                            <div class="portfolio-desc">
                                <h3><a href="/yedek-parca">Güzine gömleği</a></h3>
                            </div>
                        </div>
                    </div>

                    <div class="oc-item">
                        <div class="iportfolio">
                            <div class="portfolio-image">
                                <a href="/yedek-parca">
                                    <img src="/img/template1/yedekparca/yedek-parca-6k.jpg" alt="Console Activity">
                                </a>
                                <div class="portfolio-overlay">
                                    <a href="/img/template1/yedekparca/yedek-parca-6.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
                                    <a href="/yedek-parca" class="right-icon"><i class="icon-line-ellipsis"></i></a>
                                </div>
                            </div>
                            <div class="portfolio-desc">
                                <h3><a href="/yedek-parca">Araboru rezistansı</a></h3>
                            </div>
                        </div>
                    </div>

                    <div class="oc-item">
                        <div class="iportfolio edit-varol">
                            <div class="portfolio-image">
                                <a href="/yedek-parca">
                                    <img src="/img/template1/yedekparca/yedek-parca-7k.jpg" alt="Console Activity">
                                </a>
                                <div class="portfolio-overlay">
                                    <a href="/img/template1/yedekparca/yedek-parca-7.jpg" class="left-icon" data-lightbox="image"><i class="icon-line-plus"></i></a>
                                    <a href="/yedek-parca" class="right-icon"><i class="icon-line-ellipsis"></i></a>
                                </div>
                            </div>
                            <div class="portfolio-desc">
                                <h3><a href="/yedek-parca">Güzine</a></h3>
                            </div>
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>

</section><!-- #content end -->

@endsection